﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Common
{
    public static class Common
    {
        public const string InsertDataStartDateBlock = "InsertData.StartDate.Block";
        public const string BenchmarkingPeriodStartDateCounter = "Benchmarking.PeriodStartDate.ChangeCounter";
        public const string InsertDataStartDateNewUser = "InsertData.StartDate.NewUser";
        public const string RegisterWelcomeMessage = "Register.Welcome.Message";
        public const string ReviewOfDataRoomNightCoefficient = "ReviewOfData.RoomNight.Coefficient";
    }
}