﻿using barometer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    public class HistoryUserValue
    {
        public int Id { get; set; }
        public virtual User User { get; set; }
        public virtual Period Period { get; set; }
        public int Value { get; set; }
        public HistoryUserValueEnum Type { get; set; }
    }
}