﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    public class BarometerDbContext : IdentityDbContext<User>
    {
        public DbSet<Period> Period { get; set; }
        public DbSet<CategoryCountry> CategoryCountry { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Distance> Distance { get; set; }
        public DbSet<TypeData> TypeData { get; set; }
        public DbSet<Workload> Workload { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Data> Data { get; set; }
        public DbSet<ApplicationSetting> ApplicationSetting { get; set; }
        public DbSet<InsertDataPermission> InsertDataPermission { get; set; }
        public DbSet<HistoryUserValue> HistoryUserValue { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<TICData> TICData { get; set; }

        public static bool isMigration = false;

        public BarometerDbContext()
            : base("DB_A17495_barometr", throwIfV1Schema: false)
        {
        }
        
        public static BarometerDbContext Create()
        {
            return new BarometerDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
     }
}
