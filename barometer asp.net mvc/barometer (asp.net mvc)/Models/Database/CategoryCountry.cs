﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    public class CategoryCountry
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortPosition { get; set; }
        public DateTime Created { get; set; }
        public DateTime Changed { get; set; }
        public string UserName { get; set; }
        public bool Visible { get; set; }

        public virtual ICollection<Country> Countries { get; set; }
    }
}
