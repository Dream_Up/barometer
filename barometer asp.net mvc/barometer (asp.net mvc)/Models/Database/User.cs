﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;

namespace barometer.Models.Database
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser
    {
        public int OldId { get; set; }
        public bool Status { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public short? Stars { get; set; }
        public int UserType { get; set; }
        public double AvaragePrice { get; set; }
        public virtual Distance Distance { get; set; }
        public int? NumberOfBeds { get; set; }
        public int? NumberOfRooms { get; set; }
        public int? NumberOfConferencePosition { get; set; }
        public DateTime Created { get; set; }
        public DateTime Changed { get; set; }
        public string CreatedName { get; set; }
        public string UpdatedName { get; set; }

        public virtual ICollection<Workload> Workloads { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Data> Data { get; set; }
        public virtual ICollection<HistoryUserValue> HistoryUserValue { get; set; }
        public virtual ICollection<InsertDataPermission> InsertDataPermission { get; set; }
        public virtual ICollection<TICData> TICData { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}