﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    public class Distance
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }
        public DateTime Created { get; set; }
        public DateTime Changed { get; set; }
        public string UserName { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}