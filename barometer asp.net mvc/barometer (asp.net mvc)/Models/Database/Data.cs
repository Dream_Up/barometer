﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    public class Data
    {
        public int Id { get; set; }
        public virtual Period Period { get; set; }
        public virtual User User {get; set;}
        public int OldUserId { get; set; }
        public virtual Country Country { get; set; }
        public virtual TypeData TypeData { get; set; }
        public int Value { get; set; }
        public DateTime Created { get; set; }
        public DateTime Changed { get; set; }
    }
}