﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    [Table("TypeData")]
    public class TypeData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Visible { get; set; }
        public DateTime Created { get; set; }
        public DateTime Changed { get; set; }
        public string UserName { get; set; }

        public virtual ICollection<Data> Data { get; set; }

    }
}