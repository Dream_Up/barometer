﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    public class InsertDataPermission
    {
        public Guid Id { get; set; }
        public User User { get; set; }
        public Period Period { get; set; }
    }
}