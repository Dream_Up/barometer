﻿using barometer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    public class TICData
    {
        public int Id { get; set; }
        public int CountTourists { get; set; }
        public Sex Sex { get; set; }
        public int Events { get; set; }
        public int PrintingIndustry { get; set; }
        public int Souvenirs { get; set; }
        public int Excursions { get; set; }
        public int Museums { get; set; }
        public int Food { get; set; }
        public int ReferenceInformation { get; set; }
        public int Residence { get; set; }
        public int Transport { get; set; }
        public DateTime DateVisit { get; set; }
        public virtual User User { get; set; }
        public DateTime Created { get; set; }
        public DateTime Changed { get; set; }
        public virtual Country Country { get; set; }
        public virtual Region Region { get; set; }
        public virtual City City { get; set; }
    }
}