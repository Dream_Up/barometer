﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    public class City
    {
        public int Id { get; set; }
        public bool Visible { get; set; }
        public int SortPosition { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string UserName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Changed { get; set; }
        public virtual Region Region { get; set; }

        public virtual ICollection<TICData> TICData { get; set; }
    }
}