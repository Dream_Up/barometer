﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace barometer.Models.Database
{
    public class Period
    {
        public int Id { get; set; }
        public bool Visible { get; set; }
        public DateTime StartDate { get; set; }

        public virtual ICollection<Workload> Workloads { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Data> Data { get; set; }
        public virtual ICollection<HistoryUserValue> HistoryUserValue { get; set; }
        public virtual ICollection<InsertDataPermission> InsertDataPermission { get; set; }
    }
}