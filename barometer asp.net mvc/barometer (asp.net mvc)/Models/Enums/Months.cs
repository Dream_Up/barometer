﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace barometer.Models.Enums
{
    public enum Months
    {
        [Display(Name="СІЧЕНЬ")]
        January = 1,
        [Display(Name = "ЛЮТИЙ")]
        February = 2,
        [Display(Name = "БЕРЕЗЕНЬ")]
        March = 3,
        [Display(Name = "КВІТЕНЬ")]
        April = 4,
        [Display(Name = "ТРАВЕНЬ")]
        May = 5,
        [Display(Name = "ЧЕРВЕНЬ")]
        June = 6,
        [Display(Name = "ЛИПЕНЬ")]
        July = 7,
        [Display(Name = "СЕРПЕНЬ")]
        August = 8,
        [Display(Name = "ВЕРЕСЕНЬ")]
        September = 9,
        [Display(Name = "ЖОВТЕНЬ")]
        October = 10,
        [Display(Name = "ЛИСТОПАД")]
        November = 11,
        [Display(Name = "ГРУДЕНЬ")]
        December = 12
    }
}