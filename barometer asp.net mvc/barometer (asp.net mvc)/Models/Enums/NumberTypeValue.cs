﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Enums
{
    public enum NumberTypeValue
    {
        NumberOfRooms = 0,
        NumberOfBeds = 1
    }
}