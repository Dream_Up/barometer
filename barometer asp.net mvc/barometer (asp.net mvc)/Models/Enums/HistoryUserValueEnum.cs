﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Enums
{
    public enum HistoryUserValueEnum
    {
        NumberOfBeds,
        NumberOfRooms
    }
}