﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Enums
{
    public enum TypeDataEnum
    {
	    Hostel = 1,
	    HotelAndHostelAndTIC = 2,
	    Hotel = 3,
	    RegularAirport = 5,
	    CharterAirport = 6
    }
}