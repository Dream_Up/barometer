﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace barometer.Models.Enums
{
    public sealed class UsersType
    {
        private readonly string name;
        private readonly string value;

        public static readonly UsersType Hotel = new UsersType("1", "Готель");
        public static readonly UsersType Hostel = new UsersType("2", "Хостел");
        public static readonly UsersType Operator = new UsersType("3", "Оператор");
        public static readonly UsersType TIC = new UsersType("4", "TIC");
        public static readonly UsersType Airport = new UsersType("5", "Аеропорт");
        public static readonly UsersType Admin = new UsersType("6", "Адмін");

        private UsersType(string value, string name)
        {
            this.name = name;
            this.value = value;
        }

        public string Value 
        {
            get { return value; }
        } 

        public string Name 
        {
            get { return name; }
        } 

        public static string GetName(string value)
        {
            switch (value)
            {
                case "1":
                    return Hotel.name;
                case "2":
                    return Hostel.name;
                case "3":
                    return Operator.name;
                case "4":
                    return TIC.name;
                case "5":
                    return Airport.name;
                case "6":
                    return Admin.name;
                default:
                    return string.Empty;
            }
        }

        public static string GetValue(string value)
        {
            switch (value)
            {
                case "Hotel":
                    return Hotel.value;
                case "Hostel":
                    return Hostel.value;
                case "Operator":
                    return Operator.value;
                case "TIC":
                    return TIC.value;
                case "Airport":
                    return Airport.value;
                case "Admin":
                    return Admin.value;
                default:
                    return string.Empty;
            }
        }
    }
}