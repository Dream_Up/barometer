﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Convertables
{
    public class InfoGraphicsHeader
    {
        public int Sum { get; set; }
        public int Count { get; set; }
        public bool IsRaise { get; set; }
    }
}