﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Convertables
{
    public class TICDataClause
    {
        public int CountryId { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}