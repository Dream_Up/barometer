﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Convertables
{
    public class InfoGraphics
    {
        public string Alias { get; set; }
        public int Sum { get; set; }
    }
}