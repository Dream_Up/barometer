﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace barometer.Models.ViewModel
{
    public class CommentsViewModel
    {
        public int PeriodFirst { get; set; }
        public int PeriodSecond { get; set; }

        public IEnumerable<SelectListItem> Periods { get; set; }
    }
}