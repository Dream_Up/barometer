﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace barometer.Models.ViewModel
{
    public class EditCommentViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        [Required(ErrorMessage = "Поле 'Текст' є обов'язковим!")]
        public string Text { get; set; }
        public string UserName { get; set; }
        public int? PeriodId { get; set; }
        public string CreatedDate { get; set; }
        public string ChangedDate { get; set; }
        public IEnumerable<SelectListItem> Periods { get; set; }
    }
}