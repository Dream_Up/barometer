﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace barometer.Models.ViewModel
{
    public class BenchmarkingViewModel
    {
        public int PeriodFirst { get; set; }
        public int PeriodSecond { get; set; }
        public List<int> Distance { get; set; }
        public List<int> Star { get; set; }
        public bool IsConferenceHall { get; set; }
        public List<int> NumberOfRoom { get; set; }
        public int PeriodForComments { get; set; }
        public IEnumerable<SelectListItem> Stars { get; set; }
        public IEnumerable<SelectListItem> Distances { get; set; }
        public IEnumerable<SelectListItem> Periods { get; set; }
        public IEnumerable<SelectListItem> NumberOfRooms { get; set; }
    }
}