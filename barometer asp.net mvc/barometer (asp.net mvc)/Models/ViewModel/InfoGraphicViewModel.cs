﻿using barometer.Models.Convertables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.ViewModel
{
    public class InfoGraphicViewModel
    {
        public List<InfoGraphics> Data { get; set;}
        public List<InfoGraphicsHeader> Header { get; set; }
        public int Year { get; set; }
        public string NameMonth { get; set; }
    }
}