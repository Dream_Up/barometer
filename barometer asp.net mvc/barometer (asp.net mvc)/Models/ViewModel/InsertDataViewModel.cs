﻿using barometer.Models.Business;
using barometer.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.ViewModel
{
    public class InsertDataViewModel
    {
        public Tuple<List<int>,List<InsertDataPeriod>> Periods { get; set; }
        public DateTime PeriodDateDefault { get; set; }
    }
}