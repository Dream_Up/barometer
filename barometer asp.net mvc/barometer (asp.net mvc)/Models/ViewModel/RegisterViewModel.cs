﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace barometer.Models.ViewModel
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Поле є обов'язковим")]
        public string UserName { get; set; }

        public short Star { get; set; }

        [Required(ErrorMessage = "Поле є обов'язковим")]
        public string Name { get; set; }

        public string Surname { get; set; }

        [Required(ErrorMessage = "Поле є обов'язковим")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Введіть числове значенння")]
        public int NumberOfBeds { get; set; }

        [Required(ErrorMessage = "Поле є обов'язковим")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Введіть числове значенння")]
        public int NumberOfRooms { get; set; }

        public int DistanceId { get; set; }

        [Required(ErrorMessage = "Поле є обов'язковим")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Введіть числове значенння")]
        public int NumberOfConferencePosition { get; set; }

        public string RoleName { get; set; }

        public string PhoneNumber { get; set; }

        public IEnumerable<SelectListItem> Distances { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }
        public IEnumerable<SelectListItem> Stars { get; set; }

        [Required(ErrorMessage = "Поле є обов'язковим")]
        [EmailAddress(ErrorMessage = "Не дійсний Email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле є обов'язковим")]
        [StringLength(100, ErrorMessage = "Пароль повинен бути від 6 до 15 символів", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Поле є обов'язковим")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Поле \"пароль\" не збігаються")]
        public string ConfirmPassword { get; set; }
    }
}