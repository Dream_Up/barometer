﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace barometer.Models.ViewModel
{
    public class ReviewOfDataViewModel
    {
        public int PeriodFirst { get; set; }
        public int PeriodSecond { get; set; }
        public string UserType { get; set; }
        public List<int> Distance { get; set; }
        public List<int> Star { get; set; }
        public List<int> Country { get; set; }
        public bool IsFirst { get; set; }
        public bool IsConferenceHall { get; set; }
        public List<int> NumberOfRoom { get; set; }
        public int PeriodForComments { get; set; }
        public int ReportYear { get; set; }

        public IEnumerable<SelectListItem> Stars { get; set; }
        public IEnumerable<SelectListItem> Distances { get; set; }
        public IEnumerable<SelectListItem> UserTypes { get; set; }
        public IEnumerable<SelectListItem> Periods { get; set; }
        public IEnumerable<SelectListItem> NumberOfRooms { get; set; }
        public IEnumerable<SelectListItem> Countries { get; set; }
        public IEnumerable<SelectListItem> ReportYears { get; set; }
    }
}