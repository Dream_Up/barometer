﻿using barometer.Models.Convertables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.ViewModel
{
    public class HomeIndexViewModel
    {
        public InfoGraphicViewModel InfoGraphicViewModel { get; set; }
    }
}