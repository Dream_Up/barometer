﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace barometer.Models.ViewModel
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Поле є обов'язковим")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Поле є обов'язковим")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}