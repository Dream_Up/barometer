﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace barometer.Models.ViewModel
{
    public class FeedbackViewModel
    {
        [Required(ErrorMessage = "Поле 'Ім'я' є обов'язковим!")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Поле 'E-мейл' є обов'язковим!")]
        [EmailAddress(ErrorMessage = "Не валідний E-мейл")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Поле 'Телефон' є обов'язковим!")]
        public string Telephone { get; set; }
        [Required(ErrorMessage = "Поле 'Текст' є обов'язковим!")]
        public string Text { get; set; }
        public HttpPostedFileBase FileUpload { get; set; }

        public override string ToString()
        {
            return "<div>Name: " + Name + "</div>" +
                "<div>Email: " + Email + "</div>" +
                "<div>Telephone: " + Telephone + "</div>" +
                "<div>Text: " + Text + "</div>";
        }
    }
}