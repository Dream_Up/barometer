﻿using barometer.Models.Business;
using barometer.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.ViewModel
{
    public class InsertDataPartialViewModel
    {
        public List<DataByCountry> DataByCountry { get; set; }
        public Comment Comment { get; set; }
        public bool IsDisabled { get; set; }
        public int CountTourists { get; set; }
    }
}