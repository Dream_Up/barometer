﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Business
{
    public class DataByCountry
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string CountryAlias { get; set; }
        public int ValueOne { get; set; }
        public int TypeData { get; set; }
    }
}