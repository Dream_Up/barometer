﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Business
{
    public class ReviewOfDataChartModel
    {
        public string PeriodDate { get; set; }
        public int SumFirst { get; set; }
        public int SumSecond { get; set; }
    }
}