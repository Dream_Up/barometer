﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Business
{
    public class InsertDataSaveModel
    {
        public int PeriodId { get; set; }
        public string CommentText { get; set; }
        public Dictionary<string, ItemData> Data { get; set; }
        public int CommentId { get; set; }
    }

    public class ItemData
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public int TypeData { get; set; } 
    }
}