﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Business
{
    public class ReviewOfDataLineModel
    {
        public DateTime PeriodDate { get; set; }
        public int Sum { get; set; }
    }
}