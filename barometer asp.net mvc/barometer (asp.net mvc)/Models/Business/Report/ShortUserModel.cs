﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Business.Report
{
    public class ShortUserModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}