﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace barometer.Models.Business
{
    public class BenchmarkingHotelAndHostel
    {
        public string PeriodDate { get; set; }
        public double Workload { get; set; }
    }
}