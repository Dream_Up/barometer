﻿using barometer.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace barometer.Models.Business
{
    [NotMapped]
    public class InsertDataPeriod : Period 
    {
        public int CountTourists { get; set; } 
    }
}