﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace barometer.Extensions.HtmlHelpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString MenuActionLink(this HtmlHelper html, string text, string action, string controller)
        {
            var routeAction = html.ViewContext.RouteData.Values["action"].ToString();
            var routeController = html.ViewContext.RouteData.Values["controller"].ToString();
            if(routeAction == action && routeController == controller)
            {
                return html.ActionLink(text, action, controller, null, new { @class="active"});
            }
            else {
                return html.ActionLink(text, action, controller);
            }
        }
    }
}