﻿using barometer.Models.Convertables;
using barometer.Models.Database;
using barometer.Models.Enums;
using barometer.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using barometer.Extensions;

namespace barometer.Helpers
{
    public class InfoGraphicsHelper
    {
        public static InfoGraphicViewModel GeInfoGraphics(BarometerDbContext context, DateTime date, DateTime prevDate)
        {
            var listInfoGraphics = new List<InfoGraphics>();
            var listInfoGraphicsHeader = new List<InfoGraphicsHeader>();
            using (var connection = context.Database.Connection)
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "[dbo].[GetInfoGraphicsInData]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@PeriodDate", date));

                for (int i = 0; i < 20; i++)
                {
                    listInfoGraphics.Add(new InfoGraphics());
                }

                int count;
                using (var reader = command.ExecuteReader())
                {
                    var objectContext = ((IObjectContextAdapter)context).ObjectContext;
                    for (int i = 0; i < 4; i++)
                    {
                        count = i * 5;
                        foreach (var item in objectContext.Translate<InfoGraphics>(reader))
                        {
                            listInfoGraphics[count] = item;
                            count++;
                        }
                        reader.NextResult();
                    }


                }

                command = connection.CreateCommand();
                command.CommandText = "[dbo].[GetHeadersCurrentInfoGraphics]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@PeriodDate", date));
                command.Parameters.Add(new SqlParameter("@PrevPeriodDate", prevDate));

                using (var reader = command.ExecuteReader())
                {
                    var objectContext = ((IObjectContextAdapter)context).ObjectContext;
                    for (int i = 0; i < 8; i++)
                    {
                        listInfoGraphicsHeader.AddRange(objectContext.Translate<InfoGraphicsHeader>(reader));
                        reader.NextResult();
                    }
                }

                for (int i = 0; i < 4; i++)
                {
                    var element = listInfoGraphicsHeader[i];
                    var nextElement = listInfoGraphicsHeader[i + 1];
                    if (element.Sum <= 0 || element.Count <= 0)
                    {
                        element.IsRaise = false;
                    }
                    else if (nextElement.Sum <= 0 || nextElement.Count <= 0)
                    {
                        element.IsRaise = true;
                    }
                    else
                    {
                        element.IsRaise = ((element.Sum / element.Count) > (nextElement.Sum / nextElement.Count));
                    }
                    listInfoGraphicsHeader.RemoveAt(i + 1);
                }
                connection.Close();
            }

            return new InfoGraphicViewModel { Data = listInfoGraphics, Header = listInfoGraphicsHeader, NameMonth = ((Months)date.Month).GetDisplayName(), Year = date.Year };
        }

        public static ManageInfoGraphicsViewModel GetManageInfoGraphicsViewModel(BarometerDbContext context) 
        {
            var dateNow = DateTime.Now.AddMonths(-1);
            int periodFirst = context.Period.FirstOrDefault(x => x.StartDate.Year == dateNow.Year && x.StartDate.Month == dateNow.Month).Id;
            int periodSecond = context.Period.FirstOrDefault(x => x.StartDate.Year == dateNow.Year - 1 && x.StartDate.Month == dateNow.Month).Id;
            return new ManageInfoGraphicsViewModel { 
                Periods = CommonHelper.GetPeriods(context),
                PeriodFirst = periodFirst,
                PeriodSecond = periodSecond
            };
        }
    }
}