﻿using barometer.Models.Database;
using barometer.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using barometer.Models.Enums;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Net.Mail;
using System.Text;

namespace barometer.Helpers
{
    public static class AccountHelper
    {
        public static RegisterViewModel GetRegisterViewModel(BarometerDbContext context)
        {
            return new RegisterViewModel
            {
                RoleName = "Hotel", // need refactoring
                Distances = CommonHelper.Distances,
                Roles = CommonHelper.Roles,
                Stars = CommonHelper.Stars
            };
        }

        public static async Task<object> AddNewUser(BarometerDbContext context, RegisterViewModel model, ApplicationUserManager userManager, string userName)
        {
            var notValidMessages = new Dictionary<string, string>();

            if (context.Users.Any(x => x.Email == model.Email))
            {
                notValidMessages.Add("Email", "Такий 'Email' існує. Виберіть будь ласка інший!");
            }

            if (context.Users.Any(x => x.UserName == model.UserName))
            {
                notValidMessages.Add("UserName", "Такий 'Логін' існує. Виберіть будь ласка інший!");
            }

            if (notValidMessages.Count > 0)
            {
                return new { ValidationMessages = notValidMessages };
            }

            var user = new User
            {
                UserName = model.UserName,
                Email = model.Email,
                Name = model.Name,
                Stars = model.Star,
                Surname = model.Surname,
                NumberOfBeds = model.NumberOfBeds,
                NumberOfRooms = model.NumberOfRooms,
                NumberOfConferencePosition = model.NumberOfConferencePosition,
                PhoneNumber = model.PhoneNumber,
                Created = DateTime.Now,
                Changed = DateTime.Now,
                CreatedName = userName,
                UpdatedName = userName
            };

            var result = await userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            { 
                userManager.AddToRole(user.Id, model.RoleName);
                context.Users.Find(user.Id).Distance = context.Distance.Find(model.DistanceId);
                context.SaveChanges();

                try
                {
                    MailMessage message = new MailMessage("info@barometer.lviv.ua", model.Email)
                    {
                        Subject = "barometer.lviv.ua - Реєстрація нового користувача",
                        BodyEncoding = Encoding.UTF8,
                        Body = String.Format(CommonHelper.RegisterWelcomeMessage, model.UserName, model.Password),
                        IsBodyHtml = true,
                        SubjectEncoding = Encoding.UTF8,
                    };

                    message.Bcc.Add("touristbarometer@gmail.com");

                    SmtpClient client = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network
                    };
                    client.Send(message);
                }
                catch
                {
                    throw new Exception();
                }

                return new { SuccessMessage = "Користувач успішно зареєстрований." };
            }
            else
            {
                return new { ErrorMessage = AddErrors(result) };
            }
        }
        
        private static string AddErrors(IdentityResult result)
        {
            string errorsMessage = string.Empty;
            foreach (var error in result.Errors)
            {
                errorsMessage += error + " ";
            }
            return errorsMessage;
        }
    }
}