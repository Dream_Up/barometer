﻿using barometer.Models.Database;
using barometer.Models.Enums;
using barometer.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace barometer.Helpers
{
    public static class EditUserDataHelper
    {
        public static EditUserDataViewModel GetUserDataViewModel(BarometerDbContext context, string userName)
        {
            var userData = context.Users.FirstOrDefault(x => x.UserName == userName);

            var roleName = UsersType.GetName(userData.Roles.FirstOrDefault().RoleId);

            if (userData != null)
            {
                return new EditUserDataViewModel()
                {
                    UserName = userData.UserName,
                    DistanceId = userData.Distance != null ? userData.Distance.Id : default(int),
                    Star = userData.Stars.GetValueOrDefault(),
                    Name = userData.Name,
                    Surname = userData.Surname,
                    NumberOfBeds = userData.NumberOfBeds.GetValueOrDefault(),
                    NumberOfRooms = userData.NumberOfRooms.GetValueOrDefault(),
                    NumberOfConferencePosition = userData.NumberOfConferencePosition.GetValueOrDefault(),
                    RoleName = roleName,
                    PhoneNumber = userData.PhoneNumber,
                    Email = userData.Email,
                    Distances = CommonHelper.Distances,
                    Stars = CommonHelper.Stars
                };
            }
            else
            {
                throw new Exception();
            }
        }

        public static object SaveUserData(BarometerDbContext context, EditUserDataViewModel model, IPrincipal currentUser)
        {
            using (var transactionContext = context.Database.BeginTransaction())
            {
                try
                {
                    var user = context.Users.FirstOrDefault(x => x.UserName == currentUser.Identity.Name);
                    var period = CommonHelper.GetCurrentPeriod(context);

                    user.Email = model.Email;
                    user.Name = model.Name;
                    user.Surname = model.Surname;
                    user.PhoneNumber = model.PhoneNumber;
                    user.UpdatedName = currentUser.Identity.Name;
                    user.Changed = DateTime.Now;

                    if (currentUser.IsInRole("Hotel"))
                    {
                        user.Stars = model.Star;
                        if (user.NumberOfRooms != model.NumberOfRooms)
                        {
                            var historyItem = context.HistoryUserValue.FirstOrDefault(x => x.Period.Id == period.Id 
                                && x.User.Id == user.Id && x.Type == HistoryUserValueEnum.NumberOfRooms);

                            if (historyItem == null)
                            {
                                context.HistoryUserValue.Add(new HistoryUserValue
                                {
                                    Type = HistoryUserValueEnum.NumberOfRooms,
                                    Value = user.NumberOfRooms.GetValueOrDefault(),
                                    User = user,
                                    Period = period
                                });
                            }
                        }
                        user.NumberOfRooms = model.NumberOfRooms;
                        user.NumberOfConferencePosition = model.NumberOfConferencePosition;
                    }

                    if (currentUser.IsInRole("Hostel") || currentUser.IsInRole("Hotel"))
                    {
                        if (user.NumberOfBeds != model.NumberOfBeds)
                        {
                            var historyItem = context.HistoryUserValue.FirstOrDefault(x => x.Period.Id == period.Id
                                && x.User.Id == user.Id && x.Type == HistoryUserValueEnum.NumberOfBeds);

                            if (historyItem == null)
                            {
                                context.HistoryUserValue.Add(new HistoryUserValue
                                {
                                    Type = HistoryUserValueEnum.NumberOfBeds,
                                    Value = user.NumberOfBeds.GetValueOrDefault(),
                                    User = user,
                                    Period = period
                                });
                            }
                        }
                        user.NumberOfBeds = model.NumberOfBeds;
                    }

                    if (!currentUser.IsInRole("Operator"))
                    {
                        user.Distance = context.Distance.Find(model.DistanceId);
                    }

                    context.SaveChanges();
                    transactionContext.Commit();

                    return new { SuccessMessage = "Дані успішно збережені!" };
                }
                catch(Exception ex)
                {
                    transactionContext.Rollback();
                    return new { ErrorMessage = "Виникла помилка, оновіть сторінку!" };
                }
            }
        }
    }
}