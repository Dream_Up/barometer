﻿using barometer.Models.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Dynamic;
using System.Linq;
using LinqKit;
using System.Web;
using barometer.Extensions;
using barometer.Models.Enums;
using barometer.Models.ViewModel;
using barometer.Models.Business;
using System.Linq.Expressions;
using OfficeOpenXml;
using System.Security.Principal;
using System.Web.Mvc;
using System.IO;
using OfficeOpenXml.Style;
using System.Drawing;

namespace barometer.Helpers
{

    public static class ReviewOfDataHelper
    {
        public static IEnumerable<dynamic> GetTableData(ReviewOfDataViewModel model, BarometerDbContext context)
        {
            var periods = context.Period.Where(x => x.Id >= model.PeriodFirst && x.Id <= model.PeriodSecond).Select(x => x.Id);

            string periodsHeader = string.Empty;
            string periodsHeaderSum = string.Empty;
            string periodsParam = string.Empty;

            foreach (var period in periods)
            {
                periodsHeader += "ISNULL([" + period + "],0) AS [" + period + "]" + (period != model.PeriodSecond ? "," : string.Empty);
                periodsParam += "[" + period + "]" + (period != model.PeriodSecond ? "," : string.Empty);
                periodsHeaderSum += "ISNULL([" + period + "],0)" + (period != model.PeriodSecond ? "+" : string.Empty);

            }

            var typesData = "TypeDataId ";

            switch (model.UserType)
            {
                case "1": // UsersType.Hotel
                    typesData += "=" + ((model.IsFirst) ? ((int)TypeDataEnum.HotelAndHostelAndTIC).ToString() : ((int)TypeDataEnum.Hotel).ToString());
                    break;
                case "2": // UsersType.Hostel
                    typesData += "=" + ((model.IsFirst) ? ((int)TypeDataEnum.HotelAndHostelAndTIC).ToString() : ((int)TypeDataEnum.Hostel).ToString());
                    break;
                case "4": // UsersType.TIC
                    typesData += "=" + ((int)TypeDataEnum.HotelAndHostelAndTIC).ToString();
                    break;
                case "5": // UsersType.Airport
                    typesData += "=" + ((model.IsFirst) ? ((int)TypeDataEnum.RegularAirport).ToString() : ((int)TypeDataEnum.CharterAirport).ToString());
                    break;
            }

            var userTypes = " and RoleId = " + model.UserType;

            var countries = string.Empty;
            var distances = string.Empty;
            var numbersOfRooms = string.Empty;
            var stars = string.Empty;
            var conference = string.Empty;

            countries = " and CountryId in (" + string.Join<int>(",", model.Country) + ")";

            if (model.UserType == UsersType.Hotel.Value || model.UserType == UsersType.Hostel.Value)
            {
                distances = " and DistanceId in (" + string.Join<int>(",", model.Distance) + ")";
            }

            if (model.UserType == UsersType.Hotel.Value)
            {
                if (model.NumberOfRoom.Count() > 0)
                {
                    numbersOfRooms = " and (";
                    foreach (var item in model.NumberOfRoom.OrderBy(x => x))
                    {
                        switch (item)
                        {
                            case 0:
                                numbersOfRooms += "NumberOfRooms <= 19";
                                break;
                            case 1:
                                numbersOfRooms += (numbersOfRooms.Contains("NumberOfRooms") ? " or " : string.Empty) + "NumberOfRooms >= 20 and NumberOfRooms <= 49 ";
                                break;
                            case 2:
                                numbersOfRooms += (numbersOfRooms.Contains("NumberOfRooms") ? " or " : string.Empty) + "NumberOfRooms >= 50 and NumberOfRooms <= 89 ";
                                break;
                            case 3:
                                numbersOfRooms += (numbersOfRooms.Contains("NumberOfRooms") ? " or " : string.Empty) + "NumberOfRooms >= 90 ";
                                break;
                        }
                    }
                    numbersOfRooms += ")";
                }

                stars = " and Stars in (" + string.Join<int>(",", model.Star) + ")";
                conference = " and NumberOfConferencePosition >=" + (model.IsConferenceHall ? "50" : "0");
            }

            var result = context.Database.DynamicSqlQuery("SELECT (" + periodsHeaderSum + ") AS SumRow, CountryName, " + periodsHeader + " FROM " +
               "(select PeriodId, Value, CountryName from ReviewOfData where " + typesData + userTypes + countries + distances + stars + numbersOfRooms + conference + " ) AS SourceTable " +
               "PIVOT(Sum(Value) FOR PeriodId IN (" + periodsParam + ")) AS PivotTable ");

            return result.Cast<dynamic>().Where(x => x.SumRow > 0);
        }

        public static IEnumerable<ExportExcelModel> GetDataForExcel(int year, List<UsersType> listUserType, bool withRegion, bool isTourist, BarometerDbContext context)
        {
            var periods = context.Period.Where(x => x.StartDate.Year == year).OrderBy(x => x.StartDate).ToArray();

            string periodsHeader = string.Empty;
            string periodsParam = string.Empty;

            periodsHeader += "ISNULL([" + periods[0].Id + "],0) AS January, ";
            periodsHeader += "ISNULL([" + periods[1].Id + "],0) AS February, ";
            periodsHeader += "ISNULL([" + periods[2].Id + "],0) AS March, ";
            periodsHeader += "ISNULL([" + periods[3].Id + "],0) AS April, ";
            periodsHeader += "ISNULL([" + periods[4].Id + "],0) AS May, ";
            periodsHeader += "ISNULL([" + periods[5].Id + "],0) AS June, ";
            periodsHeader += "ISNULL([" + periods[6].Id + "],0) AS July, ";
            periodsHeader += "ISNULL([" + periods[7].Id + "],0) AS August, ";
            periodsHeader += "ISNULL([" + periods[8].Id + "],0) AS September, ";
            periodsHeader += "ISNULL([" + periods[9].Id + "],0) AS October, ";
            periodsHeader += "ISNULL([" + periods[10].Id + "],0) AS November, ";
            periodsHeader += "ISNULL([" + periods[11].Id + "],0) AS December ";

            foreach (var period in periods)
            {
                periodsParam += "[" + period.Id + "]" + (period.StartDate.Month != 12 ? "," : string.Empty);
            }

            string typesData = string.Empty, userTypes = string.Empty;
            if (listUserType?.Count > 0)
            {
                foreach (var userType in listUserType)
                {
                    if (string.IsNullOrEmpty(typesData))
                    {
                        typesData += " ( ";
                        userTypes += " ( ";
                    }
                    else
                    {
                        typesData += " or ";
                        userTypes += " or ";
                    }

                    switch (userType.Value)
                    {
                        case "1": // UsersType.Hotel
                            typesData += "TypeDataId =" + ((isTourist) ? ((int)TypeDataEnum.HotelAndHostelAndTIC).ToString() : ((int)TypeDataEnum.Hotel).ToString());
                            break;
                        case "2": // UsersType.Hostel
                            typesData += "TypeDataId =" + ((isTourist) ? ((int)TypeDataEnum.HotelAndHostelAndTIC).ToString() : ((int)TypeDataEnum.Hostel).ToString());
                            break;
                    }

                    userTypes += " RoleId =" + userType.Value;
                }
            }

            typesData += " ) ";
            userTypes += " ) ";

            var distances = string.Empty;

            if (!withRegion)
            {
                distances = " and DistanceId != 4 ";
            }

            var result = context.Database.SqlQuery<ExportExcelModel>("SELECT CountryId, CategoryId, " + periodsHeader + " FROM " +
               "(select PeriodId, Value, CountryId, CategoryId from ReviewOfData where " + typesData + " and " + userTypes + distances + " ) AS SourceTable " +
               "PIVOT(Sum(Value) FOR PeriodId IN (" + periodsParam + ")) AS PivotTable ");

            return result.ToList<ExportExcelModel>();
        }


        public static Tuple<string, string> GetTableColumn(int periodFirst, int periodSecond, BarometerDbContext context)
        {
            string htmlColumns = "<th>Країна</th>";
            string jsColumns = "[{ \"data\": \"CountryName\" },";
            var periods = context.Period.Where(x => x.Id >= periodFirst && x.Id <= periodSecond).Select(x => new { Id = x.Id, Date = x.StartDate });

            foreach (var period in periods)
            {
                htmlColumns += "<th>" + ((Months)period.Date.Month).GetDisplayName() + "</br>" + period.Date.Year.ToString() + "</th>";
                jsColumns += "{ \"data\": " + period.Id + " }" + (period.Id != periodSecond ? "," : string.Empty);
            }

            jsColumns += "]";

            return new Tuple<string, string>(htmlColumns, jsColumns);
        }

        public static ReviewOfDataViewModel GetReviewOfDataViewModel(BarometerDbContext context, string roleName)
        {
            var dateNow = DateTime.Now.AddMonths(-1);
            int periodFirst = context.Period.FirstOrDefault(x => x.StartDate.Year == dateNow.Year - 1 && x.StartDate.Month == dateNow.Month).Id;
            int periodCurrent = context.Period.FirstOrDefault(x => x.StartDate.Year == dateNow.Year && x.StartDate.Month == dateNow.Month).Id;
            return new ReviewOfDataViewModel
            {
                Periods = CommonHelper.GetPeriods(context),
                PeriodFirst = periodFirst,
                PeriodSecond = periodCurrent,
                UserTypes = CommonHelper.UserTypes,
                UserType = UsersType.GetValue(roleName == "Operator" ? "Hotel" : roleName),
                Distances = CommonHelper.Distances,
                Stars = CommonHelper.Stars,
                NumberOfRooms = CommonHelper.NumberOfRooms,
                Countries = CommonHelper.Countries,
                PeriodForComments = periodCurrent,
                ReportYears = context.Period.Where(x => x.StartDate.Year <= DateTime.Now.Year)
                    .Select(x => new SelectListItem { Text = x.StartDate.Year.ToString(), Value = x.StartDate.Year.ToString() })
                    .Distinct().OrderByDescending(x => x.Value)
            };
        }

        public static IEnumerable<ReviewOfDataChartModel> GetDataChart(BarometerDbContext context, ReviewOfDataViewModel model)
        {
            List<ReviewOfDataLineModel> listFirst = new List<ReviewOfDataLineModel>();
            List<ReviewOfDataLineModel> listSecond = new List<ReviewOfDataLineModel>();

            if (model.UserType == UsersType.Hotel.Value)
            {
                listFirst = GetDataLine(context, model, TypeDataEnum.HotelAndHostelAndTIC);
                listSecond = GetDataLine(context, model, TypeDataEnum.Hotel);
            }
            else if (model.UserType == UsersType.Hostel.Value)
            {
                listFirst = GetDataLine(context, model, TypeDataEnum.HotelAndHostelAndTIC);
                listSecond = GetDataLine(context, model, TypeDataEnum.Hostel);
            }
            else if (model.UserType == UsersType.Airport.Value)
            {
                listFirst = GetDataLine(context, model, TypeDataEnum.RegularAirport);
                listSecond = GetDataLine(context, model, TypeDataEnum.CharterAirport);
            }
            else if (model.UserType == UsersType.TIC.Value)
            {
                listFirst = GetDataLine(context, model, TypeDataEnum.HotelAndHostelAndTIC);
            }

            foreach (var item in listFirst.OrderBy(x => x.PeriodDate))
            {
                yield return new ReviewOfDataChartModel
                {
                    PeriodDate = item.PeriodDate.ToString("MM.yyyy"),
                    SumFirst = item.Sum,
                    SumSecond = listSecond != null && listSecond.Any(x => x.PeriodDate == item.PeriodDate)
                        ? listSecond.FirstOrDefault(x => x.PeriodDate == item.PeriodDate).Sum : 0
                };
            }
        }

        public static List<ReviewOfDataLineModel> GetDataLine(BarometerDbContext context, ReviewOfDataViewModel model, TypeDataEnum typeData)
        {
            Expression<Func<Data, bool>> expressionData = x => x.Period.Id >= model.PeriodFirst && x.Period.Id <= model.PeriodSecond && x.TypeData.Id == (short)typeData && model.Country.Contains(x.Country.Id) && x.User.Roles.Any(z => z.RoleId == model.UserType);

            if (model.UserType == UsersType.Hostel.Value || model.UserType == UsersType.Hotel.Value)
            {
                expressionData = expressionData.AndAlso(x => model.Distance.Contains(x.User.Distance.Id));
            }

            if (model.UserType == UsersType.Hotel.Value)
            {
                if (model.IsConferenceHall)
                {
                    expressionData = expressionData.AndAlso(x => x.User.NumberOfConferencePosition >= 50);
                }

                expressionData = expressionData.AndAlso(CommonHelper.GetNumberOfRoomsExpression(model.NumberOfRoom));
                expressionData = expressionData.AndAlso(x => model.Star.Contains((int)x.User.Stars));
            }

            var data = context.Data.AsExpandable().Where(expressionData).GroupBy(x => new { x.Period })
            .Select(x => new ReviewOfDataLineModel
            {
                PeriodDate = x.Key.Period.StartDate,
                Sum = x.Sum(y => y.Value)
            }).ToList();

            return data;
        }

        public static byte[] GetReviewOfDataReport(int year, BarometerDbContext context)
        {
            using (ExcelPackage xlPackage = new ExcelPackage())
            {
                ExcelWorksheet tourists = xlPackage.Workbook.Worksheets.Add("Кількість туристів");
                ExcelWorksheet bedNights = xlPackage.Workbook.Worksheets.Add("Кількість ліжко-ночей");

                CreateReviewOfDataWorksheets(tourists, year, true, context);
                CreateReviewOfDataWorksheets(bedNights, year, false, context);

                xlPackage.Workbook.Calculate();
                return xlPackage.GetAsByteArray();
            }

        }

        public static void CreateReviewOfDataWorksheets(ExcelWorksheet worksheet, int year, bool isTourists, BarometerDbContext context)
        {
            var getHotelWithoutRegion = GetDataForExcel(year, new List<UsersType>() { UsersType.Hotel }, false, isTourists, context);
            var getHotelWithRegion = GetDataForExcel(year, new List<UsersType>() { UsersType.Hotel }, true, isTourists, context);

            if (!isTourists)
            {
                GetDataWithCoefficient(getHotelWithoutRegion, CommonHelper.ReviewOfDataRoomNightCoefficient);
                GetDataWithCoefficient(getHotelWithRegion, CommonHelper.ReviewOfDataRoomNightCoefficient);
            }

            var getHostelWithoutRegion = GetDataForExcel(year, new List<UsersType>() { UsersType.Hostel }, false, isTourists, context);
            var getHostelWithRegion = GetDataForExcel(year, new List<UsersType>() { UsersType.Hostel }, true, isTourists, context);
            var getHotelAndHostelWithoutRegion = MergeHotelAndHostel(getHotelWithoutRegion, getHostelWithoutRegion, isTourists);
            var getHotelAndHostelWithRegion = MergeHotelAndHostel(getHotelWithRegion, getHostelWithRegion, isTourists);

            var countries = CommonHelper.FullCountriesData;
            var categories = CommonHelper.CategoryCountry;
            var countRow = countries.Where(x => x.IsDisplayedInExcel).Count() + 32;
            var countColumns = 61;

            ExportExcelModel[] oneRowData = new ExportExcelModel[5];
            List<int> numbersSumRow = new List<int>();

            worksheet.Column(1).Width = 25;

            worksheet.Cells[14, 1, 14, countColumns].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            worksheet.Cells[14, 1, 14, countColumns].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(216, 228, 188));
            worksheet.Cells[14, 1, 14, countColumns].Style.Font.Bold = true;

            worksheet.Cells[14, 1, 16, 1].Merge = true;
            worksheet.Cells[14, 1, 16, 1].Style.Font.Bold = true;
            worksheet.Cells[14, 1, 16, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Cells[14, 1, 16, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[14, 1, 16, 1].Style.WrapText = true;
            worksheet.Cells[14, 1, 16, 1].Value = "Країни";

            worksheet.Row(15).Height = worksheet.Row(16).Height = 23;

            CreateInfoTable(2, 4, worksheet);
            CreateInfoTable(6, 8, worksheet);

            worksheet.Cells[14, 1, countRow + 1, countColumns].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            worksheet.Cells[14, 1, countRow + 1, countColumns].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            worksheet.Cells[14, 1, countRow + 1, countColumns].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            worksheet.Cells[14, 1, countRow + 1, countColumns].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            worksheet.Cells[14, 1, countRow + 1, countColumns].Style.Border.Right.Color.SetColor(Color.FromArgb(155, 187, 89));
            worksheet.Cells[14, 1, countRow + 1, countColumns].Style.Border.Left.Color.SetColor(Color.FromArgb(155, 187, 89));
            worksheet.Cells[14, 1, countRow + 1, countColumns].Style.Border.Top.Color.SetColor(Color.FromArgb(155, 187, 89));
            worksheet.Cells[14, 1, countRow + 1, countColumns].Style.Border.Bottom.Color.SetColor(Color.FromArgb(155, 187, 89));

            for (int i = 2, m = 1; m <= 12; i += 5, m++)
            {
                worksheet.Cells[14, i, 14, i + 4].Merge = true;
                worksheet.Cells[14, i, 14, i + 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[14, i, 14, i + 4].Value = ((Months)(m)).GetDisplayName();

                worksheet.Cells[15, i, 16, i].Value = "Готелі Львів";
                worksheet.Cells[15, i + 1, 16, i + 1].Value = "Готелі загалом";
                worksheet.Cells[15, i + 2, 16, i + 2].Value = "Готелі + Хостели Львів";
                worksheet.Cells[15, i + 3, 16, i + 3].Value = "Готелі + Хостели загалом";
                worksheet.Cells[15, i + 4, 16, i + 4].Value = "Хостели";

                for (int j = i; j <= i + 4; j++)
                {
                    worksheet.Cells[15, j, 16, j].Merge = true;
                    worksheet.Cells[15, j, 16, j].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[15, j, 16, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[15, j, 16, j].Style.WrapText = true;
                }

                worksheet.Cells[14, i, countRow + 1, i].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                worksheet.Cells[14, i, countRow + 1, i].Style.Border.Left.Color.SetColor(Color.FromArgb(155, 187, 89));
            }

            worksheet.Cells[14, countColumns, countRow + 1, countColumns].Style.Border.Right.Style = ExcelBorderStyle.Medium;
            worksheet.Cells[14, countColumns, countRow + 1, countColumns].Style.Border.Right.Color.SetColor(Color.FromArgb(155, 187, 89));

            int startIndex, n = 17;
            foreach (var category in categories.OrderBy(x => x.SortPosition))
            {
                worksheet.Cells[n, 1, n, 1].Value = category.Name;
                worksheet.Cells[n, 1, n, 1].Style.Font.Bold = true;
                worksheet.Cells[n, 1, n, countColumns].Merge = true;
                worksheet.Cells[n, 1, n, countColumns].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[n, 1, n, countColumns].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(192, 192, 192));

                setStyleRow(++n, worksheet);
                startIndex = n;
                var numberRowOther = n + countries.Where(x => x.Category.Id == category.Id && x.IsDisplayedInExcel).Count();

                if (category.Id != 6)
                {
                    numbersSumRow.Add(numberRowOther + 1);
                }
                else
                {
                    numbersSumRow.Add(numberRowOther - 1);
                }

                foreach (var country in countries.Where(x => x.Category.Id == category.Id).OrderBy(x => x.SortPosition))
                {
                    oneRowData[0] = getHotelWithoutRegion.FirstOrDefault(x => x.CountryId == Convert.ToInt32(country.Id));
                    oneRowData[1] = getHotelWithRegion.FirstOrDefault(x => x.CountryId == Convert.ToInt32(country.Id));
                    oneRowData[2] = getHotelAndHostelWithoutRegion.FirstOrDefault(x => x.CountryId == Convert.ToInt32(country.Id));
                    oneRowData[3] = getHotelAndHostelWithRegion.FirstOrDefault(x => x.CountryId == Convert.ToInt32(country.Id));
                    oneRowData[4] = getHostelWithRegion.FirstOrDefault(x => x.CountryId == Convert.ToInt32(country.Id));

                    if (country.IsDisplayedInExcel)
                    {
                        worksheet.Cells[n, 1, n, 1].Value = country.Name;
                        setValues(n, 2, oneRowData[0], worksheet);
                        setValues(n, 3, oneRowData[1], worksheet);
                        setValues(n, 4, oneRowData[2], worksheet);
                        setValues(n, 5, oneRowData[3], worksheet);
                        setValues(n, 6, oneRowData[4], worksheet);
                        setStyleRow(++n, worksheet);
                    }
                    else if (category.Id != 6)
                    {
                        setCategoryOther(numberRowOther, 2, oneRowData[0], worksheet);
                        setCategoryOther(numberRowOther, 3, oneRowData[1], worksheet);
                        setCategoryOther(numberRowOther, 4, oneRowData[2], worksheet);
                        setCategoryOther(numberRowOther, 5, oneRowData[3], worksheet);
                        setCategoryOther(numberRowOther, 6, oneRowData[4], worksheet);
                    }
                }

                if (category.Id != 6)
                {
                    setStyleRow(++n, worksheet);
                    worksheet.Cells[numberRowOther, 1, numberRowOther, 1].Value = category.Name + " інші";
                    setStyleRow(++n, worksheet);
                    worksheet.Cells[numberRowOther + 1, 1, numberRowOther + 1, 1].Value = "Сума";
                    worksheet.Cells[numberRowOther + 1, 1, numberRowOther + 1, countColumns].Style.Font.Bold = true;

                    for (int j = 2; j <= countColumns; j++)
                    {
                        worksheet.Cells[numberRowOther + 1, j, numberRowOther + 1, j].Formula = "=SUM(" +
                            worksheet.Cells[startIndex, j, startIndex, j].Address + ":" +
                            worksheet.Cells[numberRowOther, j, numberRowOther, j].Address + ")";
                    }
                }
            }

            // Вставка загальної суми
            worksheet.Cells[countRow + 1, 1, countRow + 1, 1].Value = "Загальна сума";
            worksheet.Row(countRow + 1).Style.Font.Bold = true;

            string sumOperator = "+";
            for (int j = 2; j <= countColumns; j++)
            {
                worksheet.Cells[countRow + 1, j, countRow + 1, j].Formula = "=";

                foreach (var item in numbersSumRow)
                {
                    worksheet.Cells[countRow + 1, j, countRow + 1, j].Formula += worksheet.Cells[item, j, item, j].Address +
                    (item != numbersSumRow.LastOrDefault() ? "+" : string.Empty);
                }

                worksheet.Cells[countRow + 1, j, countRow + 1, j].Formula += "";
            }
        }
        public static void setValues(int i, int j, ExportExcelModel item, ExcelWorksheet worksheet)
        {
            worksheet.Cells[i, j, i, j].Value = item != null ? item.January : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.February : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.March : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.April : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.May : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.June : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.July : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.August : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.September : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.October : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.November : 0;
            j += 5;
            worksheet.Cells[i, j, i, j].Value = item != null ? item.December : 0;
        }

        public static void setCategoryOther(int i, int j, ExportExcelModel item, ExcelWorksheet worksheet)
        {
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.January : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.February : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.March : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.April : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.May : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.June : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.July : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.August : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.September : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.October : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.November : 0);
            j += 5;
            worksheet.Cells[i, j, i, j].Value = (int)(worksheet.Cells[i, j, i, j].Value != null ? worksheet.Cells[i, j, i, j].Value : 0) + (item != null ? item.December : 0);
        }

        public static void setStyleRow(int n, ExcelWorksheet tourists)
        {
            if (n % 2 != 0)
            {
                tourists.Cells[n, 1, n, 61].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                tourists.Cells[n, 1, n, 61].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(216, 228, 188));
            }
        }

        public static IEnumerable<ExportExcelModel> MergeHotelAndHostel(IEnumerable<ExportExcelModel> hotelsData, IEnumerable<ExportExcelModel> hostelsData, bool isTourists)
        {
            ExportExcelModel hotel;
            ExportExcelModel hostel;

            foreach (var country in CommonHelper.FullCountriesData)
            {
                hotel = hotelsData.FirstOrDefault(x => x.CountryId == country.Id);
                hostel = hostelsData.FirstOrDefault(x => x.CountryId == country.Id);

                yield return new ExportExcelModel
                {
                    January = (hotel != null ? hotel.January : 0) + (hostel != null ? hostel.January : 0),
                    February = (hotel != null ? hotel.February : 0) + (hostel != null ? hostel.February : 0),
                    March = (hotel != null ? hotel.March : 0) + (hostel != null ? hostel.March : 0),
                    April = (hotel != null ? hotel.April : 0) + (hostel != null ? hostel.April : 0),
                    May = (hotel != null ? hotel.May : 0) + (hostel != null ? hostel.May : 0),
                    June = (hotel != null ? hotel.June : 0) + (hostel != null ? hostel.June : 0),
                    July = (hotel != null ? hotel.July : 0) + (hostel != null ? hostel.July : 0),
                    August = (hotel != null ? hotel.August : 0) + (hostel != null ? hostel.August : 0),
                    September = (hotel != null ? hotel.September : 0) + (hostel != null ? hostel.September : 0),
                    October = (hotel != null ? hotel.October : 0) + (hostel != null ? hostel.October : 0),
                    November = (hotel != null ? hotel.November : 0) + (hostel != null ? hostel.November : 0),
                    December = (hotel != null ? hotel.December : 0) + (hostel != null ? hostel.December : 0),
                    CategoryId = country.Category.Id,
                    CountryId = country.Id
                };
            }
        }

        public static void GetDataWithCoefficient(IEnumerable<ExportExcelModel> data, double coefficient)
        {
            foreach (var item in data)
            {
                item.January = (int)Math.Ceiling(item.January * coefficient);
                item.February = (int)Math.Ceiling(item.February * coefficient);
                item.March = (int)Math.Ceiling(item.March * coefficient);
                item.April = (int)Math.Ceiling(item.April * coefficient);
                item.May = (int)Math.Ceiling(item.May * coefficient);
                item.June = (int)Math.Ceiling(item.June * coefficient);
                item.July = (int)Math.Ceiling(item.July * coefficient);
                item.August = (int)Math.Ceiling(item.August * coefficient);
                item.September = (int)Math.Ceiling(item.September * coefficient);
                item.October = (int)Math.Ceiling(item.October * coefficient);
                item.November = (int)Math.Ceiling(item.November * coefficient);
                item.December = (int)Math.Ceiling(item.December * coefficient);
            }
        }

        public static void CreateInfoTable(int start, int end, ExcelWorksheet worksheet)
        {
            worksheet.Cells[2, start, 11, start].Style.Border.Left.Style = ExcelBorderStyle.Medium;
            worksheet.Cells[2, end, 11, end].Style.Border.Right.Style = ExcelBorderStyle.Medium;
            worksheet.Cells[2, start, 2, end].Style.Border.Top.Style = ExcelBorderStyle.Medium;
            worksheet.Cells[11, start, 11, end].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
            worksheet.Cells[2, start, 11, start].Style.Border.Left.Color.SetColor(Color.FromArgb(155, 187, 89));
            worksheet.Cells[2, end, 11, end].Style.Border.Right.Color.SetColor(Color.FromArgb(155, 187, 89));
            worksheet.Cells[2, start, 2, end].Style.Border.Top.Color.SetColor(Color.FromArgb(155, 187, 89));
            worksheet.Cells[11, start, 11, end].Style.Border.Bottom.Color.SetColor(Color.FromArgb(155, 187, 89));

            worksheet.Cells[2, start, 3, end].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
            worksheet.Cells[2, start, 3, end].Style.Border.Bottom.Color.SetColor(Color.FromArgb(155, 187, 89));

            worksheet.Cells[4, start, 10, end].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            worksheet.Cells[4, start, 10, end].Style.Border.Bottom.Color.SetColor(Color.FromArgb(155, 187, 89));

            worksheet.Cells[4, end, 11, end].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            worksheet.Cells[4, end, 11, end].Style.Border.Left.Color.SetColor(Color.FromArgb(155, 187, 89));
        }
    }
}