﻿using barometer.Models.Database;
using barometer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using barometer.Extensions;

namespace barometer.Helpers
{
    public class StatisticsHelper
    {
        public static IEnumerable<object> GetStatisticsByPeriod(int periodId, BarometerDbContext context)
        {
            foreach (var user in context.Users.Where(x => x.Roles.FirstOrDefault().RoleId != UsersType.Operator.Value &&
                 x.Roles.FirstOrDefault().RoleId != UsersType.Admin.Value))
            {
                yield return new
                {
                    Type = UsersType.GetName(user.Roles.FirstOrDefault().RoleId),
                    Stars = user.Stars,
                    Name = user.UserName,
                    PersonName = user.Surname + " " + user.Name,
                    Phone = user.PhoneNumber,
                    Email = user.Email,
                    NumberOfRooms = user.NumberOfRooms,
                    NumberOfBeds = user.NumberOfBeds,
                    LastUpdatedDate = GetLastUpdatedDate(periodId, user.Id, context)
                };
            } 
        }

        private static string GetLastUpdatedDate(int periodId, string userId, BarometerDbContext context)
        {
            var data = context.Data.FirstOrDefault(x => x.Period.Id == periodId && x.User.Id == userId);
            return data != null ? data.Changed.ToString("dd.MM.yyyy") : string.Empty;
        }
    }
}