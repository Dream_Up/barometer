﻿using barometer.Extensions;
using barometer.Models.Database;
using LinqKit;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace barometer.Helpers
{
    public static class TICReportHelper
    {
        public static object GetGeographyOfVisitorsCountry(BarometerDbContext context, DateTime fromDate, DateTime toDate, string userId)
        {
            var expression = GetExpression(fromDate, toDate, userId, false, false);
            var list = context.TICData.AsExpandable().Where(expression).GroupBy(x => x.Country.Name)
                .Select(x => new { Name = x.Key, Tourist = x.Sum(y => y.CountTourists) })
                .Where(x => x.Name != null && x.Name != string.Empty && x.Tourist > 0)
                .OrderByDescending(x => x.Tourist).ToList();
            var listForReport = list.Take(9).ToList();
            var sum = list.Sum(x => x.Tourist);
            var otherSum = list.Skip(9).Sum(x => x.Tourist);

            listForReport.Add(new { Name = "Інші країни", Tourist = otherSum });
            listForReport.Add(new { Name = "Загалом", Tourist = sum });

            return new {
                Pie = listForReport,
                Table = list.Skip(9),
                TotalinP = (sum != 0 &&  otherSum != 0 ? otherSum / (sum / 100.00) : 0).ToString("0.0"),
                TotalOther = otherSum,
                Previous = (listForReport.Count == 11) ? listForReport[8].Tourist : 0
            };
        }

        public static object GetGeographyOfVisitorsCityAndRegion(BarometerDbContext context, DateTime fromDate, DateTime toDate, string userId)
        {
            var expression = GetExpression(fromDate, toDate, userId, true, false);
            var regionExpression = expression.AndAlso(x => x.City == null);
            var listViaRegion = context.TICData.AsExpandable().Where(regionExpression).GroupBy(x => x.Region.Name).ToList();
            var listViaCity = context.TICData.AsExpandable().Where(expression).GroupBy(x => x.City.Name).ToList();
            var list = listViaRegion.Union(listViaCity)
                .Select(x => new { Name = x.Key, Tourist = x.Sum(y => y.CountTourists) })
                .Where(x => x.Name != null && x.Name != string.Empty && x.Tourist > 0)
                .OrderByDescending(x => x.Tourist);
            var listForReport = list.Take(9).ToList();
            var sum = list.Sum(x => x.Tourist);
            var otherSum = list.Skip(9).Sum(x => x.Tourist);

            listForReport.Add(new { Name = "Інші міста", Tourist = otherSum });
            listForReport.Add(new { Name = "Загалом", Tourist = sum });

            return new {
                Pie = listForReport,
                Table = list.Skip(9),
                TotalinP = (sum != 0 && otherSum != 0 ? otherSum / (sum / 100.00) : 0).ToString("0.0"),
                TotalOther = otherSum,
                Previous = (listForReport.Count == 11) ? listForReport[8].Tourist : 0
            };
        }

        public static object GetInteterestsOfVisitors(BarometerDbContext context, DateTime fromDate, DateTime toDate, string userId)
        {
            var expression = GetExpression(fromDate, toDate, userId, false, false);
            var list = context.TICData.AsExpandable().Where(expression).ToList();

            var result = new List<dynamic> {
                new { Name = "Музеї", Tourist = list.Sum(x => x.Museums)},
                new { Name = "Друкована продукція", Tourist = list.Sum(x => x.PrintingIndustry)},
                new { Name = "Події", Tourist = list.Sum(x => x.Events)},
                new { Name = "Сувеніри", Tourist = list.Sum(x => x.Souvenirs)},
                new { Name = "Екскурсії", Tourist = list.Sum(x => x.Excursions)},
                new { Name = "Харчування", Tourist = list.Sum(x => x.Food)},
                new { Name = "Довідкова інформація", Tourist = list.Sum(x => x.ReferenceInformation)},
                new { Name = "Проживання", Tourist = list.Sum(x => x.Residence)},
                new { Name = "Транспорт", Tourist = list.Sum(x => x.Transport)},
            };

            result = result.Where(x => x.Tourist > 0).OrderByDescending(x => x.Tourist).ToList();
            result.Add(new { Name = "Загалом", Tourist = result.Sum(x => x.Tourist) });

            return result;
        }

        public static object GetComparisonOfVisitors(BarometerDbContext context, DateTime fromDate, DateTime toDate, string userId, bool isForCountry)
        {
            IEnumerable<dynamic> list;
            Expression<Func<TICData, bool>> expression;
            if (isForCountry)
            {
                expression = GetExpression(fromDate, toDate, userId, false, true);
                list = context.TICData.AsExpandable()
               .Where(expression)
               .GroupBy(x => new { x.Country, x.DateVisit.Year })
               .Select(x => new { Name = x.Key.Country.Name, Year = x.Key.Year, Tourist = x.Sum(y => y.CountTourists) }).ToList();
            }
            else
            {
                expression = GetExpression(fromDate, toDate, userId, true, true);
                var regionExpression = expression.AndAlso(x => x.City == null);
                var listViaRegion = context.TICData.AsExpandable().Where(regionExpression).GroupBy(x => new { x.Region.Name, x.DateVisit.Year }).ToList();
                var listViaCity = context.TICData.AsExpandable().Where(expression).GroupBy(x => new { x.City.Name, x.DateVisit.Year }).ToList();
                list = listViaRegion.Union(listViaCity)
                .Select(x => new { Name = x.Key.Name, Year = x.Key.Year, Tourist = x.Sum(y => y.CountTourists) })
                .Where(x => x.Name != null && x.Name != string.Empty).ToList();
            }

            var result = new List<dynamic>();
            foreach (var item in list.Where(x => x.Year == fromDate.Year))
            {
                var ticRow = list.FirstOrDefault(x => x.Year == fromDate.AddYears(-1).Year && x.Name == item.Name);
                var touristPrev = (ticRow != null) ? ticRow.Tourist : 0;
                result.Add(
                    new {
                        Name = item.Name,
                        TouristsNow = item.Tourist,
                        TouristsPrev = touristPrev,
                        Increase = GetIncrease(item.Tourist, touristPrev)
                    });
            }

            var sumNow = list.Where(x => x.Year == fromDate.Year).Sum(x => x.Tourist);
            var sumPrev = list.Where(x => x.Year == fromDate.AddYears(-1).Year).Sum(x => x.Tourist);
            result = result.OrderByDescending(x => x.TouristsNow).Take(7).ToList();
            result.Add(
                    new
                    {
                        Name = "Загалом за період",
                        TouristsNow = sumNow,
                        TouristsPrev = sumPrev,
                        Increase = GetIncrease(sumNow, sumPrev)
                    });

            return new {
                Column1 = fromDate.Year,
                Column2 = fromDate.AddYears(-1).Year,
                ColumnChart = result
            };
        }

        public static Tuple<DateTime, DateTime> ConvertToDateTime(string from, string to)
        {
            from += " 00:00:00";
            to += " 23:59:59";

            DateTime fromDate = DateTime.ParseExact(from, "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime toDate = DateTime.ParseExact(to, "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);

            return new Tuple<DateTime, DateTime>(fromDate, toDate);
        }

        private static Expression<Func<TICData, bool>> GetExpression(DateTime fromDate, DateTime toDate, string userId, bool isCity, bool isComparison)
        {
            DateTime prevFromDate;
            DateTime prevToDate;
            Expression<Func<TICData, bool>> expression;

            if (isComparison)
            {
                prevFromDate = fromDate.AddYears(-1);
                prevToDate = toDate.AddYears(-1);
                expression = x => (x.DateVisit >= fromDate && x.DateVisit <= toDate) || 
                (x.DateVisit >= prevFromDate && x.DateVisit <= prevToDate);
            }
            else
            {
                expression = x => x.DateVisit >= fromDate && x.DateVisit <= toDate;
            }

            if (isCity)
            {
                expression.AndAlso(x => x.Country.Alias == "ukraine");
            }

            if (!string.IsNullOrEmpty(userId))
            {
                expression = expression.AndAlso(x => x.User.Id == userId);
            }
            return expression;
        }

        private static double GetIncrease(double touristsNow, double touristsPrev)
        {
            if(touristsNow == 0 && touristsPrev == 0)
            {
                return 0;
            }
            else if (touristsNow == 0)
            {
                return -100;
            }
            else if (touristsPrev == 0)
            {
                return 100;
            }
            else {
                return (touristsNow / touristsPrev-1) * 100;
            }
        }
    }
}