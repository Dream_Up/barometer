﻿using barometer.Models.Common;
using barometer.Models.Database;
using barometer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Principal;
using barometer.Extensions;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace barometer.Helpers
{
    public static class CommonHelper
    {
        static CommonHelper()
        {
            var context = new BarometerDbContext();

            UserTypes = new List<SelectListItem>() {
                new SelectListItem { Text = "Готель", Value = UsersType.Hotel.Value},
                new SelectListItem { Text = "Хостел", Value = UsersType.Hostel.Value},
                new SelectListItem { Text = "ТІЦ", Value = UsersType.TIC.Value},
                new SelectListItem { Text = "Аеропорт", Value = UsersType.Airport.Value},
            };

            Distances = context.Distance.Select(x => new SelectListItem { Text = x.Description, Value = x.Id.ToString() });

            Stars = new List<SelectListItem>() {
                new SelectListItem { Text = "Без зірок", Value = "0"},
                new SelectListItem { Text = "2*", Value = "2"},
                new SelectListItem { Text = "3*", Value = "3"},
                new SelectListItem { Text = "4*", Value = "4"},
                new SelectListItem { Text = "5*", Value = "5"},
            };

            NumberOfRooms = new List<SelectListItem>() {
                new SelectListItem { Text = "до 19", Value = "0"},
                new SelectListItem { Text = "від 20 до 49", Value = "1"},
                new SelectListItem { Text = "від 50 до 89 ", Value = "2"},
                new SelectListItem { Text = "від 90 і більше", Value = "3"}
            };

            FullCountriesData = context.Country.ToList();

            CategoryCountry = context.CategoryCountry.ToList();

            Countries = FullCountriesData.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

            InsertDataStartDateBlock = DateTime.ParseExact(context.ApplicationSetting.FirstOrDefault(x => x.Key == Common.InsertDataStartDateBlock).Value, "d.MM.yyyy", null);

            var coefficientData = context.ApplicationSetting.FirstOrDefault(x => x.Key == Common.ReviewOfDataRoomNightCoefficient);
            ReviewOfDataRoomNightCoefficient = ConvertStringToDouble(coefficientData.Value);

            BenchmarkingPeriodStartDateCounter = DateTime.ParseExact(context.ApplicationSetting.FirstOrDefault(x => x.Key == Common.BenchmarkingPeriodStartDateCounter).Value, "d.MM.yyyy", null);

            InsertDataStartDateNewUser = DateTime.ParseExact(context.ApplicationSetting.FirstOrDefault(x => x.Key == Common.InsertDataStartDateNewUser).Value, "d.MM.yyyy", null);

            Roles = GetRolesWithDescription(context);

            RegisterWelcomeMessage = context.ApplicationSetting.FirstOrDefault(x => x.Key == Common.RegisterWelcomeMessage).Value;

        }

        public static DateTime DatePeriod {
            get {
                var dateNow = DateTime.Now;
                return dateNow.AddMonths(-1);
            }
        }

        public static string RegisterWelcomeMessage { get;set; }

        public static IEnumerable<SelectListItem> Roles { get; set; }

        public static DateTime InsertDataStartDateBlock { get; set; }

        public static DateTime BenchmarkingPeriodStartDateCounter { get; set; }

        public static DateTime InsertDataStartDateNewUser { get; set; }

        public static double ReviewOfDataRoomNightCoefficient { get; set; }

        public static IEnumerable<SelectListItem> UserTypes { get; set; }

        public static IEnumerable<SelectListItem> Distances { get; set; }

        public static IEnumerable<SelectListItem> Stars { get; set; }

        public static IEnumerable<SelectListItem> NumberOfRooms { get; set; }

        public static IEnumerable<SelectListItem> Countries { get; set; }

        public static IEnumerable<Country> FullCountriesData { get; set; }

        public static IEnumerable<CategoryCountry> CategoryCountry { get; set; }

        public static Period GetCurrentPeriod(BarometerDbContext context)
        {
            return context.Period.FirstOrDefault(x => x.StartDate.Month == DatePeriod.Month && x.StartDate.Year == DatePeriod.Year);
        }

        private static double ConvertStringToDouble(string str)
        {
            Double result;
            if(!Double.TryParse(str, out result))
            {
                var isDotExist = str.Contains(".");
                var oldDivider = isDotExist ? "." : ",";
                var newDivider = !isDotExist ? "." : ",";
                result = Double.Parse(str.Replace(oldDivider, newDivider));
            }

            return result;
        }

        public static IEnumerable<SelectListItem> GetPeriods(BarometerDbContext context)
        {
            return context.Period.Where(x => x.StartDate <= DatePeriod).OrderByDescending(x => x.StartDate)
                .Select(x => new SelectListItem
                {
                    Text = (x.StartDate.Month < 10 ? "0" + x.StartDate.Month.ToString() : x.StartDate.Month.ToString()) + "-" + x.StartDate.Year.ToString(),
                    Value = x.Id.ToString()
                });
        }

        public static string GetFullName(string userName)
        {
            var context = new BarometerDbContext();
            var user = context.Users.FirstOrDefault(x=>x.UserName == userName);
            return user.Name + " " + user.Surname;
        }

        private static IEnumerable<SelectListItem> GetRolesWithDescription(BarometerDbContext context)
        {
            foreach (var item in context.Roles.Where(x => x.Name != "Admin"))
            {
                yield return new SelectListItem
                {
                    Text = UsersType.GetName(item.Id),
                    Value = item.Name
                };
            }
        }

        public static short GetInsertDataOtherDataType(IPrincipal user)
        {
            if(user.IsInRole("Hotel"))
            {
                return (short)TypeDataEnum.Hotel;
            }
            
            if(user.IsInRole("Airport"))
            {
                return (short)TypeDataEnum.CharterAirport;
            }

            if (user.IsInRole("Hostel"))
            {
                return (short)TypeDataEnum.Hostel;
            }

            return 0;
        }

        public static Expression<Func<Data, bool>> GetNumberOfRoomsExpression(List<int> numberOfRoom)
        {
            Expression<Func<Data, bool>> expression = null;
            Expression<Func<Data, bool>> expressionClause = null;

            foreach (var item in numberOfRoom)
            {
                switch (item)
                {
                    case 0:
                        if (expression != null)
                        {
                            expression = expression.OrElse(x => x.User.NumberOfRooms <= 19);
                        }
                        else
                        {
                            expression = x => x.User.NumberOfRooms <= 19;
                        }
                        break;
                    case 1:
                        if (expression != null)
                        {
                            expression = expression.OrElse(expressionClause = x => x.User.NumberOfRooms >= 20 && x.User.NumberOfRooms <= 49);
                        }
                        else
                        {
                            expression = x => x.User.NumberOfRooms >= 20 && x.User.NumberOfRooms <= 49;
                        }
                        break;
                    case 2:
                        if (expression != null)
                        {
                            expression = expression.OrElse(x => x.User.NumberOfRooms >= 50 && x.User.NumberOfRooms <= 89);
                        }
                        else
                        {
                            expression = x => x.User.NumberOfRooms >= 50 && x.User.NumberOfRooms <= 89;
                        }
                        break;
                    case 3:
                        if (expression != null)
                        {
                            expression = expression.OrElse(x => x.User.NumberOfRooms >= 90);
                        }
                        else
                        {
                            expression = x => x.User.NumberOfRooms >= 90;
                        }
                        break;
                }
            }
            return expression;
        }
    }
}