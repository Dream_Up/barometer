﻿using barometer.Models.Business;
using barometer.Models.Database;
using barometer.Models.Enums;
using barometer.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using LinqKit;
using barometer.Extensions;
using System.Security.Principal;

namespace barometer.Helpers
{
    public static class BenchmarkingHelper
    {
        public static BenchmarkingViewModel GetBenchmarkingViewModel(BarometerDbContext context)
        {
            var dateNow = DateTime.Now.AddMonths(-1);
            int periodFirst = context.Period.FirstOrDefault(x => x.StartDate.Year == dateNow.Year - 1 && x.StartDate.Month == dateNow.Month).Id;
            int periodCurrent = context.Period.FirstOrDefault(x => x.StartDate.Year == dateNow.Year && x.StartDate.Month == dateNow.Month).Id;
            return new BenchmarkingViewModel
            {
                PeriodFirst = periodFirst,
                PeriodSecond = periodCurrent,
                Distances = CommonHelper.Distances,
                Stars = CommonHelper.Stars,
                NumberOfRooms = CommonHelper.NumberOfRooms,
                PeriodForComments = periodCurrent,
                Periods = CommonHelper.GetPeriods(context)
            };
        }

        public class BenchmarkingChart
        {
            public DateTime PeriodDate { get; set; }
            public string UserId { get; set; }
            public double Sum { get; set; }
        }

        public static IEnumerable<object> GetBenchmarkingChartData(BarometerDbContext context, BenchmarkingViewModel model, IPrincipal user)
        {
            IEnumerable<BenchmarkingHotelAndHostel> convertListOne, convertListTwo = null;
            if (user.IsInRole("Hotel"))
            {
                convertListOne = GetBenchmarkingHotelAndHostelOwnData(context, model, user.Identity.Name, UsersType.Hotel.Value);
                convertListTwo = GetBenchmarkingHotelAndHostelData(context, model, UsersType.Hotel.Value);
            }
            else if (user.IsInRole("Hostel"))
            {
                convertListOne = GetBenchmarkingHotelAndHostelOwnData(context, model, user.Identity.Name, UsersType.Hostel.Value);
                convertListTwo = GetBenchmarkingHotelAndHostelData(context, model, UsersType.Hostel.Value);
            }
            else
            {
                convertListOne = GetBenchmarkingHotelAndHostelData(context, model, UsersType.Hotel.Value);
                convertListTwo = GetBenchmarkingHotelAndHostelData(context, model, UsersType.Hostel.Value);
            }

            var convertList = new List<object>();
            foreach (var item in convertListOne)
            {
                convertList.Add(new
                {
                    PeriodDate = item.PeriodDate,
                    WorkloadItem1 = item.Workload,
                    WorkloadItem2 = convertListTwo.FirstOrDefault(x => x.PeriodDate == item.PeriodDate).Workload
                });
            }

            return convertList;
        }


        private static IEnumerable<BenchmarkingHotelAndHostel> GetBenchmarkingHotelAndHostelData(BarometerDbContext context, BenchmarkingViewModel model, string userTypeValue)
        {
            double workload;
            Expression<Func<User, bool>> expressionUsers = x => x.Roles.Any(z => z.RoleId == userTypeValue);
            Expression<Func<Data, bool>> expressionData = x => x.TypeData.Id == (int)((UsersType.Hostel.Value != userTypeValue) ? TypeDataEnum.Hotel : TypeDataEnum.Hostel)
               && x.User.Roles.Any(z => z.RoleId == userTypeValue) && x.Period.Id >= model.PeriodFirst && x.Period.Id <= model.PeriodSecond && model.Distance.Contains(x.User.Distance.Id);

            if (UsersType.Hostel.Value != userTypeValue)
            {
                if (model.IsConferenceHall)
                {
                    expressionData = expressionData.AndAlso(x => x.User.NumberOfConferencePosition >= 50);
                    expressionUsers = expressionUsers.AndAlso(x => x.NumberOfConferencePosition >= 50);
                }

                expressionData = expressionData.AndAlso(CommonHelper.GetNumberOfRoomsExpression(model.NumberOfRoom));
                expressionData = expressionData.AndAlso(x => model.Star.Contains((int)x.User.Stars));
                expressionUsers = expressionUsers.AndAlso(GetNumberOfRoomsExpressionForUsers(model));
            }

            var data = context.Data.AsExpandable().Where(expressionData).OrderBy(x => x.Period.StartDate)
                .GroupBy(x => new { x.Period.StartDate, x.User.UserName })
                .Select(x => new { PeriodDate = x.Key.StartDate, UserName = x.Key.UserName, Sum = x.Sum(y => y.Value) }).ToList();

            var listPeriod = context.Period.Where(x => x.Id >= model.PeriodFirst && x.Id <= model.PeriodSecond).ToList();
            var userlistDetails = context.Users.AsExpandable().Where(expressionUsers)
                .Select(x => new { UserName = x.UserName, NumberOfBeds = x.NumberOfBeds, NumberOfRooms = x.NumberOfRooms });

            var convertList = new List<BenchmarkingHotelAndHostel>();
            int countUsers;
            double sum;
            int numberOfBedsOrRooms;

            foreach (var itemPeriod in listPeriod)
            {
                sum = 0;
                countUsers = userlistDetails.Count();
                foreach (var itemUserDetails in userlistDetails)
                {
                    var item = data.FirstOrDefault(x => x.PeriodDate.ToString() == itemPeriod.StartDate.ToString()
                        && x.UserName == itemUserDetails.UserName);

                    if (item != null)
                    {

                        numberOfBedsOrRooms = (itemPeriod.StartDate < CommonHelper.BenchmarkingPeriodStartDateCounter && UsersType.Hostel.Value != userTypeValue) ? itemUserDetails.NumberOfRooms.GetValueOrDefault() : itemUserDetails.NumberOfBeds.GetValueOrDefault();

                        if (numberOfBedsOrRooms != 0 && item.Sum != 0)
                        {
                            sum += item.Sum / (double)(numberOfBedsOrRooms * DateTime.DaysInMonth(itemPeriod.StartDate.Year, itemPeriod.StartDate.Month)) * 100;
                        }
                        else
                        {
                            countUsers--;
                        }
                    }
                    else
                    {
                        countUsers--;
                    }
                }

                var benchmarkingHotelAndHostel = new BenchmarkingHotelAndHostel();
                benchmarkingHotelAndHostel.PeriodDate = itemPeriod.StartDate.ToString("MM.yyyy");
                workload = countUsers != 0 ? Math.Ceiling(sum / countUsers) : 0;
                benchmarkingHotelAndHostel.Workload = workload <= 100 ? workload : 100 ;

                convertList.Add(benchmarkingHotelAndHostel);
            }

            return convertList;
        }

        private static IEnumerable<BenchmarkingHotelAndHostel> GetBenchmarkingHotelAndHostelOwnData(BarometerDbContext context, BenchmarkingViewModel model, string userName, string userTypeValue)
        {
            double workload;
            int numberOfBedsOrRooms;
            var convertList = new List<BenchmarkingHotelAndHostel>();
            var listPeriod = context.Period.Where(x => x.Id >= model.PeriodFirst && x.Id <= model.PeriodSecond).ToList();
            var dbUser = context.Users.FirstOrDefault(x => x.UserName == userName);

            var data = context.Data.AsExpandable().Where(x => x.TypeData.Id == (int)((UsersType.Hostel.Value != userTypeValue) ? TypeDataEnum.Hotel : TypeDataEnum.Hostel)
            && x.User.Roles.Any(z => z.RoleId == userTypeValue) && x.Period.Id >= model.PeriodFirst && x.Period.Id <= model.PeriodSecond && x.User.UserName == userName).OrderBy(x => x.Period.StartDate)
             .GroupBy(x => new { x.Period.StartDate })
             .Select(x => new { PeriodDate = x.Key.StartDate, Sum = x.Sum(y => y.Value) }).ToList();

            double sum;

            foreach (var itemPeriod in listPeriod)
            {
                numberOfBedsOrRooms = (itemPeriod.StartDate < CommonHelper.BenchmarkingPeriodStartDateCounter && UsersType.Hostel.Value != userTypeValue) ? dbUser.NumberOfRooms.GetValueOrDefault() : dbUser.NumberOfBeds.GetValueOrDefault();

                if (numberOfBedsOrRooms != 0)
                {
                    sum = 0;

                    var item = data.FirstOrDefault(x => x.PeriodDate.ToString() == itemPeriod.StartDate.ToString());

                    if (item != null && item.Sum != 0)
                    {

                        sum += item.Sum / (double)(numberOfBedsOrRooms * DateTime.DaysInMonth(itemPeriod.StartDate.Year, itemPeriod.StartDate.Month)) * 100;
                    }

                    var benchmarkingHotelAndHostel = new BenchmarkingHotelAndHostel();
                    benchmarkingHotelAndHostel.PeriodDate = itemPeriod.StartDate.ToString("MM.yyyy");
                    workload = Math.Ceiling(sum);
                    benchmarkingHotelAndHostel.Workload = workload <= 100 ? workload : 100;

                    convertList.Add(benchmarkingHotelAndHostel);
                }
                else
                {
                    convertList.Add(new BenchmarkingHotelAndHostel
                    {
                        PeriodDate = itemPeriod.StartDate.ToString("MM.yyyy")
                    });
                }
            }

            return convertList;
        }

        private static Expression<Func<User, bool>> GetNumberOfRoomsExpressionForUsers(BenchmarkingViewModel model)
        {
            Expression<Func<User, bool>> expression = null;

            foreach (var item in model.NumberOfRoom)
            {
                switch (item)
                {
                    case 0:
                        if (expression != null)
                        {
                            expression = expression.OrElse(x => x.NumberOfRooms <= 19);
                        }
                        else
                        {
                            expression = x => x.NumberOfRooms <= 19;
                        }
                        break;
                    case 1:
                        if (expression != null)
                        {
                            expression = expression.OrElse(x => x.NumberOfRooms >= 20 && x.NumberOfRooms <= 49);
                        }
                        else
                        {
                            expression = x => x.NumberOfRooms >= 20 && x.NumberOfRooms <= 49;
                        }
                        break;
                    case 2:
                        if (expression != null)
                        {
                            expression = expression.OrElse(x => x.NumberOfRooms >= 50 && x.NumberOfRooms <= 89);
                        }
                        else
                        {
                            expression = x => x.NumberOfRooms >= 50 && x.NumberOfRooms <= 89;
                        }
                        break;
                    case 3:
                        if (expression != null)
                        {
                            expression = expression.OrElse(x => x.NumberOfRooms >= 90);
                        }
                        else
                        {
                            expression = x => x.NumberOfRooms >= 90;
                        }
                        break;
                }
            }
            return expression;
        }

    }
}