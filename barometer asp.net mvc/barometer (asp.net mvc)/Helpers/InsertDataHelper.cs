﻿using barometer.Models.Business;
using barometer.Models.Database;
using barometer.Models.Enums;
using barometer.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace barometer.Helpers
{
    public static class InsertDataHelper
    {
        public static List<DataByCountry> GetDataByPeriodAndName(BarometerDbContext context, IPrincipal user, int periodId)
        {
            var list = new List<DataByCountry>();
            TypeDataEnum oneType = 0;
            TypeDataEnum secondType = 0;

            if(user.IsInRole("Airport"))
            {
                oneType = TypeDataEnum.RegularAirport;
                secondType = TypeDataEnum.CharterAirport;
            }

            if (user.IsInRole("Hostel"))
            {
                oneType = TypeDataEnum.HotelAndHostelAndTIC;
                secondType = TypeDataEnum.Hostel;
            }
            
            if (user.IsInRole("Hotel"))
            {
                oneType = TypeDataEnum.HotelAndHostelAndTIC;
                secondType = TypeDataEnum.Hotel;
            }

            if (user.IsInRole("TIC"))
            {
                list.AddRange(GetDataByType(context, user.Identity.Name, periodId, TypeDataEnum.HotelAndHostelAndTIC));
            }
            else
            {
                list.AddRange(GetDataByType(context, user.Identity.Name, periodId, oneType));
                list.AddRange(GetDataByType(context, user.Identity.Name, periodId, secondType));
            }

            return list;
        }

        private static List<DataByCountry> GetDataByType(BarometerDbContext context, string userName, int periodId, TypeDataEnum typeData)
        {
            return (from country in context.Country
                        join data in
                            (from dataw in context.Data where dataw.User.UserName == userName && dataw.Period.Id == periodId &&
                             dataw.TypeData.Id == (short)typeData select dataw) 
                        on country.Id equals data.Country.Id into lj
                from result in lj.DefaultIfEmpty()
                orderby country.Name
                select new DataByCountry {
                    Id = result != null ? result.Id : 0,
                    CountryName = country.Name,
                    CountryAlias = country.Alias,
                    ValueOne = result != null ? result.Value : 0,
                    TypeData = (short)typeData
            }).ToList<DataByCountry>();
        }

        public static Comment GetComment(BarometerDbContext context, string userName, int periodId)
        {
            var comment = context.Comment.FirstOrDefault(x => x.User.UserName == userName && x.Period.Id == periodId);
            return comment ?? new Comment();
        }

        public static InsertDataPartialViewModel GetInsertDataPartialViewModel(BarometerDbContext context, IPrincipal user, int periodId)
        {
            var typeDataId = TypeDataIdForCountTourists(context, user);
            var model = new InsertDataPartialViewModel {
                DataByCountry = GetDataByPeriodAndName(context, user, periodId),
                Comment = GetComment(context, user.Identity.Name, periodId),
                IsDisabled = user.IsInRole("TIC") || !GetInsertDataPermmision(context, periodId, user.Identity.Name),
                CountTourists = context.Data.Where(x => x.User.UserName == user.Identity.Name 
                    && x.TypeData.Id == typeDataId && x.Period.Id == periodId)
                    .GroupBy(x => x.Period.Id).Select(x => x.Sum(y => y.Value)).FirstOrDefault()
            };

            return model;
        }

        public static bool GetInsertDataPermmision(BarometerDbContext context, int periodId, string userName)
        {
            var periodStartDate = context.Period.FirstOrDefault(x=>x.Id == periodId).StartDate;
            var specialAllow = context.InsertDataPermission.Any(x => x.User.UserName == userName && x.Period.Id == periodId);
            return specialAllow || periodStartDate >= CommonHelper.InsertDataStartDateBlock;
        }

        public static InsertDataViewModel GetInsertDataViewModel(BarometerDbContext context, IPrincipal user)
        {
           var dateTimeNow = DateTime.Now.AddMonths(-1);
           return new InsertDataViewModel {
               Periods = GetPeriods(context, user, dateTimeNow),
               PeriodDateDefault = dateTimeNow
            };
        }


        private static Tuple<List<int>,List<InsertDataPeriod>> GetPeriods(BarometerDbContext context, IPrincipal user, DateTime date)
        {
            var minStartDate = context.Data.Where(u => u.User.UserName == user.Identity.Name).Min(x => (DateTime?)x.Period.StartDate) ?? CommonHelper.InsertDataStartDateNewUser;
            short typeDataId = TypeDataIdForCountTourists(context, user);

            List<InsertDataPeriod> periods = context.Period.Where(x => x.StartDate >= minStartDate && x.StartDate <= date).Select(x => new InsertDataPeriod
            { 
                Id = x.Id,
                StartDate = x.StartDate,
                CountTourists = x.Data.Where(z => z.User.UserName == user.Identity.Name && z.TypeData.Id == typeDataId)
                                .GroupBy(z => z.Period.Id).Select(z => z.Sum(y => y.Value)).FirstOrDefault()
            }).ToList();

            return new Tuple<List<int>, List<InsertDataPeriod>> (periods.GroupBy(x => x.StartDate.Year).Select(x=>x.Key).ToList(), periods);
        }

        public static void Save(InsertDataSaveModel model, BarometerDbContext context, IPrincipal user)
        {
            using (var transactionContext = context.Database.BeginTransaction())
            {
                try
                {
                    bool isFirstSave = !context.Data.Any(x => x.User.UserName == user.Identity.Name && x.Period.Id == model.PeriodId);
                    var dbUser = context.Users.SingleOrDefault(x=>x.UserName == user.Identity.Name);
                    var period = context.Period.FirstOrDefault(x=>x.Id == model.PeriodId);

                    foreach(var item in model.Data)
                    {
                        if (item.Value.Id != 0 || item.Value.Value != 0)
                        {
                            Data data = null;
                            string countryAlias = item.Key.Substring(0, item.Key.Length - 6);
                            data = context.Data.FirstOrDefault(x => x.Id == item.Value.Id);

                            if (data != null)
                            {
                                data.Value = item.Value.Value;
                                data.Changed = DateTime.Now;
                            }
                            else
                            {
                                data = new Data();

                                data.Period = period;
                                data.User = dbUser;
                                data.Country = context.Country.FirstOrDefault(x => x.Alias == countryAlias);
                                data.TypeData = context.TypeData.FirstOrDefault(x => x.Id == item.Value.TypeData);
                                data.Created = DateTime.Now;
                                data.Changed = DateTime.Now;
                                data.Value = item.Value.Value;
 
                                context.Data.Add(data);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(model.CommentText))
                    {   
                        Comment comment = null;

                        comment = context.Comment.FirstOrDefault(x => x.Id == model.CommentId);
                        
                        if (comment != null)
                        {
                            comment.Changed = DateTime.Now;
                            comment.Text = model.CommentText;
                        }
                        else
                        {
                            comment = new Comment();

                            comment.Created = DateTime.Now;
                            comment.Changed = DateTime.Now;
                            comment.Period = period;
                            comment.Text = model.CommentText;
                            comment.User = dbUser;

                            context.Comment.Add(comment);
                        }
                    }
                    context.SaveChanges();
                    transactionContext.Commit();
                }
                catch(Exception ex)
                {
                    transactionContext.Rollback();
                    throw ex;
                }
            }
        }

        private static short TypeDataIdForCountTourists(BarometerDbContext context, IPrincipal user)
        {
            return user.IsInRole("Hostel") || user.IsInRole("Hotel") || user.IsInRole("TIC") ? (short)TypeDataEnum.HotelAndHostelAndTIC : (short)TypeDataEnum.RegularAirport;
        }
    }
}