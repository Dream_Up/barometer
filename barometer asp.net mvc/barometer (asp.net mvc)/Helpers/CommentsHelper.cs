﻿using barometer.Models.Database;
using barometer.Models.Enums;
using barometer.Models.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace barometer.Helpers
{
    public static class CommentsHelper
    {
        public static IEnumerable<string> GetCommentsByPeriod(int periodId, BarometerDbContext context)
        {
           var result = context.Comment.Where(x => x.Period.Id == periodId)
               .Select(x=> "<li class='list-group-item'>" + x.Text.Replace("\r\n", "</br>").Replace("\\n","</br>") + "</li>")
               .DefaultIfEmpty("<li class='list-group-item text-center'>Коментарі відсутні</li>");
           return result;
        }

        public static IEnumerable<object> GetCommentsByPeriods(int periodFirst, int periodSecond, BarometerDbContext context)
        {
            var result = context.Comment.Where(x => x.Period.Id >= periodFirst && x.Period.Id <= periodSecond);

            foreach (var item in result)
	        {
		        yield return new 
                {  
                    UserName = item.User.UserName, 
                    TypeDescription = UsersType.GetName(item.User.Roles.FirstOrDefault().RoleId),
                    Text = item.Text.Replace("\r\n", "</br>").Replace("\\n", "</br>"),
                    Created = item.Created.ToString("dd.MM.yyyy"),
                    Changed = item.Changed.ToString("dd.MM.yyyy"),
                    Period = item.Period.StartDate.ToString("dd.MM.yyyy"),
                    Action = GetButtonHtml(item.Id)
                };
	        }
        }

        public static CommentsViewModel GetCommentsViewModel(BarometerDbContext context)
        {
            var dateFirst = DateTime.Now.AddMonths(-6);
            var dateSecond = DateTime.Now;
            return new CommentsViewModel() 
            {
                PeriodFirst = context.Period.FirstOrDefault(x => x.StartDate.Year == dateFirst.Year && x.StartDate.Month == dateFirst.Month).Id,
                PeriodSecond = context.Period.FirstOrDefault(x => x.StartDate.Year == dateSecond.Year && x.StartDate.Month == dateSecond.Month).Id,
                Periods = CommonHelper.GetPeriods(context)
            };
        }

        public static EditCommentViewModel GetEditCommentViewModel(int id, BarometerDbContext context, string userName, bool isOperator)
        {
            if (id == 0)
            {
                return new EditCommentViewModel() 
                {
                    UserName = userName,
                    Type = isOperator ? "Оператор" : "Адміністратор",
                    Periods = CommonHelper.GetPeriods(context) 
                };
            }
            else
            {
            var comment = context.Comment.Find(id);
                return new EditCommentViewModel
                {
                    Id = comment.Id,
                    Text = comment.Text,
                    Type = UsersType.GetName(comment.User.Roles.FirstOrDefault().RoleId),
                    UserName = comment.User.UserName,
                    PeriodId = comment.Period.Id,
                    CreatedDate = comment.Created.ToString("dd.MM.yyyy"),
                    ChangedDate = comment.Changed.ToString("dd.MM.yyyy"),
                    Periods = CommonHelper.GetPeriods(context)
                };
            }
        }

        private static string GetButtonHtml(int id)
        {
            return "<div class=\"btn-group\">" +
              "<button type=\"button\" class=\"btn btn-primary\">Дії</button>"+
              "<button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">" +
                "<span class=\"caret\"></span>" +
                "<span class=\"sr-only\">Toggle Dropdown</span>" +
              "</button>" +
              "<ul class=\"dropdown-menu dropdown-menu-right dropdown-selector\" data-id=" + id + ">" +
                "<li class='edit'><a href=\"javascript:void(0)\">Редагувати</a></li>" +
                "<li class='delete'><a href=\"javascript:void(0)\">Видалити</a></li>" +
              "</ul>" +
            "</div>";
        }

        public static void RemoveComment(int id, BarometerDbContext context)
        {
            var entity = context.Comment.Find(id);
            context.Comment.Remove(entity);
            context.SaveChanges();
        }

        public static void SaveComment(EditCommentViewModel model, BarometerDbContext context, string userName)
        {
            if (model.Id == 0)
            {
                context.Comment.Add(new Comment
                {
                    Text = model.Text,
                    Period = context.Period.Find(model.PeriodId),
                    User = context.Users.FirstOrDefault(x=>x.UserName == userName),
                    Created = DateTime.Now,
                    Changed = DateTime.Now
                });
                context.SaveChanges();
            }
            else
            {
                var entity = context.Comment.Find(model.Id);
                
                entity.Text = model.Text;
                entity.Changed = DateTime.Now;

                context.SaveChanges();
            }
        }
    }
}