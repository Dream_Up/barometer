﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(barometer.Startup))]
namespace barometer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
