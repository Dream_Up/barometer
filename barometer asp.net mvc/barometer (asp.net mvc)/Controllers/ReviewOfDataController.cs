﻿using barometer.Helpers;
using barometer.Models.Database;
using barometer.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace barometer.Controllers
{
    public class ReviewOfDataController : Controller
    {
        private readonly BarometerDbContext context;

        public ReviewOfDataController()
        {
            context = new BarometerDbContext();
        }

        [HttpPost]
        public JsonResult GetTableData(ReviewOfDataViewModel model)
        {
            return Json(new
            {
                data = ReviewOfDataHelper.GetTableData(model, context)
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetTableColumn(int periodFirst, int periodSecond)
        {
            return Json(ReviewOfDataHelper.GetTableColumn(periodFirst, periodSecond, context), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Operator,Admin")]
        [HttpPost]
        public ActionResult GetReviewOfDataReport(int year)
        {
            var file = File(ReviewOfDataHelper.GetReviewOfDataReport(year, context), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Огляд даних - " + year + ".xlsx");
            Response.AddHeader("Set-Cookie", "filedownload=true; path=/; expires=5000;");
            return file;
        }
    }
}