﻿using barometer.Helpers;
using barometer.Models.Business.Report;
using barometer.Models.Convertables;
using barometer.Models.Database;
using barometer.Models.Enums;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace barometer.Controllers
{
    [Authorize(Roles = "TIC")]
    public class TICDataController : Controller
    {
        private readonly BarometerDbContext context;
        public TICDataController()
        {
            context = new BarometerDbContext();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadReport()
        {
            if (Request.Files.Count > 0)
            {
                var extension = Path.GetExtension(Request.Files[0].FileName).ToLower();
                if (extension != ".csv" && extension != ".xls" && extension != ".xlsx")
                {
                    return Json(new { WarningMessage = "Допустимі формати xls/xlsx/csv!" });
                }

                using (var transactionContext = context.Database.BeginTransaction())
                {
                    try
                    {
                        HttpPostedFileBase file = Request.Files[0];
                        string fileName = file.FileName;
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var listNotExsist = new List<Tuple<int, string>>();
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            var workCells = workSheet.Cells;
                            var listForUpdate = new List<TICDataClause>();
                            var user = context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
                            string separator = CultureInfo.CurrentCulture.DateTimeFormat.DateSeparator;
                            var startDate = new DateTime(2012, 7,1);
                            int suborderEmpty = 0;
                            TICData ticData = null;
                            string outName = null;
                            Data dbData = null;

                            for (int rowIterator = 1; rowIterator <= noOfRow; rowIterator++)
                            {
                                outName = (workCells[rowIterator, 1].Text ?? string.Empty).Trim();
                                var country = context.Country.FirstOrDefault(x => x.Name == outName);
                                var region = context.Region.FirstOrDefault(x => x.Name == outName);
                                var city = context.City.FirstOrDefault(x => x.Name == outName);
                                if (country != null || region != null || city != null)
                                {
                                    var dateTime = DateTime.ParseExact(workCells[rowIterator, 13].Text, "dd" + separator + "MM" + separator + "yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                                    ticData = context.TICData.FirstOrDefault(x => x.DateVisit.Equals(dateTime) && x.User.UserName == User.Identity.Name);

                                    if (ticData == null)
                                    {
                                        ticData = new TICData();
                                        ticData.Created = DateTime.Now;
                                        context.TICData.Add(ticData);
                                    }

                                    ticData.City = city;
                                    ticData.Region = (region == null && ticData.City != null) ? ticData.City.Region : region;
                                    ticData.Country = (country == null && ticData.Region != null) ? ticData.Region.Country : country;
                                    ticData.CountTourists = Convert.ToInt32(workCells[rowIterator, 2].Value);
                                    ticData.Sex = Convert.ToString(workCells[rowIterator, 3].Value) == "ч" ? Sex.Male : Convert.ToString(workCells[rowIterator, 3].Value) == "ж" ? Sex.Female : Sex.MaleFemale;
                                    ticData.Events = Convert.ToInt32(workCells[rowIterator, 4].Value);
                                    ticData.PrintingIndustry = Convert.ToInt32(workCells[rowIterator, 5].Value);
                                    ticData.Souvenirs = Convert.ToInt32(workCells[rowIterator, 6].Value);
                                    ticData.Excursions = Convert.ToInt32(workCells[rowIterator, 7].Value);
                                    ticData.Museums = Convert.ToInt32(workCells[rowIterator, 8].Value);
                                    ticData.Food = Convert.ToInt32(workCells[rowIterator, 9].Value);
                                    ticData.ReferenceInformation = Convert.ToInt32(workCells[rowIterator, 10].Value);
                                    ticData.Residence = Convert.ToInt32(workCells[rowIterator, 11].Value);
                                    ticData.Transport = Convert.ToInt32(workCells[rowIterator, 12].Value);
                                    ticData.DateVisit = dateTime;
                                    ticData.Changed = DateTime.Now;
                                    ticData.User = user;

                                    if (ticData.DateVisit >= startDate && !listForUpdate.Any(x => x.CountryId == ticData.Country.Id && x.Month == ticData.DateVisit.Month
                                       && x.Year == ticData.DateVisit.Year))
                                    {
                                        listForUpdate.Add(new TICDataClause()
                                        {
                                            CountryId = ticData.Country.Id,
                                            Month = ticData.DateVisit.Month,
                                            Year = ticData.DateVisit.Year
                                        });
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(outName))
                                    {
                                        if (suborderEmpty + 1 <= 10)
                                        {
                                            suborderEmpty++;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        suborderEmpty = 0;
                                    }
                                    listNotExsist.Add(new Tuple<int, string>(rowIterator, outName));
                                }
                            }
                            context.SaveChanges();

                            int sum = 0;
                            Data dataRow = null;
                            foreach (var item in listForUpdate)
                            {
                                sum = context.TICData.Where(x => x.Country.Id == item.CountryId && x.DateVisit.Month == item.Month
                                       && x.DateVisit.Year == item.Year && x.User.UserName == user.UserName).Sum(x => x.CountTourists);
                                dataRow = context.Data.FirstOrDefault(x => x.Country.Id == item.CountryId && x.Period.StartDate.Month == item.Month
                                       && x.Period.StartDate.Year == item.Year && x.User.UserName == user.UserName);

                                if (dataRow != null)
                                {
                                    dataRow.Value = sum;
                                    dataRow.Changed = DateTime.Now;
                                }
                                else
                                {
                                    dataRow = new Data();

                                    dataRow.Period = context.Period.FirstOrDefault(x => x.StartDate.Month == item.Month
                                    && x.StartDate.Year == item.Year);
                                    dataRow.User = user;
                                    dataRow.Country = context.Country.FirstOrDefault(x => x.Id == item.CountryId);
                                    dataRow.Value = sum;
                                    dataRow.TypeData = context.TypeData.FirstOrDefault(x => x.Id == (short)TypeDataEnum.HotelAndHostelAndTIC);
                                    dataRow.Created = DateTime.Now;
                                    dataRow.Changed = DateTime.Now;

                                    context.Data.Add(dataRow);
                                }
                            }

                            context.SaveChanges();
                            transactionContext.Commit();
                        }
                        return Json(new { SuccessMessage = "Файл успішно імпортованно!", ListNotExsist = listNotExsist });
                    }
                    catch (Exception ex)
                    {
                        transactionContext.Rollback();
                        return Json(new { ErrorMessage = "Помилка: " + ex.Message });
                    }
                }
            }
            else
            {
                return Json(new { WarningMessage = "Файл не існує!"});

            }

        }

        public ActionResult Report(string from, string to)
        {
            ViewBag.fromDate = from;
            ViewBag.toDate = to;
            ViewBag.Image = General.BACKGROUND_IMG_TIC_REPORTS;
            ViewBag.FirstPageImage = General.FIRST_PAGE_IMG_TIC_REPORTS;
            ViewBag.ReportLogo = General.REPORT_LOGO;
            ViewBag.ReportHeaderLogo = General.REPORT_HEADER_LOGO;
            var listUserIds = context.Roles.FirstOrDefault(x => x.Name == "TIC").Users.Select(x => x.UserId).ToList<string>();
            ViewBag.Users = context.Users.Where(x => listUserIds.Contains(x.Id)).Select(x => new ShortUserModel { UserId = x.Id, UserName = x.UserName }).ToList(); 
            return View();
        }

        public ActionResult IndividualReport(string from, string to)
        {
            ViewBag.fromDate = from;
            ViewBag.toDate = to;
            ViewBag.Image = General.BACKGROUND_IMG_TIC_REPORTS;
            ViewBag.FirstPageImage = General.FIRST_PAGE_IMG_TIC_REPORTS;
            ViewBag.UserId = context.Users.Single(x => x.UserName == User.Identity.Name).Id;
            ViewBag.ReportLogo = General.REPORT_LOGO;
            ViewBag.ReportHeaderLogo = General.REPORT_HEADER_LOGO;
            return View();
        }

        public JsonResult GetGeographyOfVisitors(string from, string to, string userId, bool isForCountry)
        {
            var period = TICReportHelper.ConvertToDateTime(from, to);
            if (isForCountry)
            {
                return Json(TICReportHelper.GetGeographyOfVisitorsCountry(context, period.Item1, period.Item2, userId));
            }
            else
            {
                return Json(TICReportHelper.GetGeographyOfVisitorsCityAndRegion(context, period.Item1, period.Item2, userId));
            }
        }

        public JsonResult GetInterestsOfVisitors(string from, string to, string userId)
        {
            var period = TICReportHelper.ConvertToDateTime(from, to);

            return Json(TICReportHelper.GetInteterestsOfVisitors(context, period.Item1, period.Item2, userId));
        }

        public JsonResult GetComparisonOfVistors(string from, string to, string userId, bool isForCountry)
        {
            var period = TICReportHelper.ConvertToDateTime(from, to);

            return Json(TICReportHelper.GetComparisonOfVisitors(context, period.Item1, period.Item2, userId, isForCountry));
        }
    }
}