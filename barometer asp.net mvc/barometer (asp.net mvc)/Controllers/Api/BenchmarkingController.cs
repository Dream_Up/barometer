﻿using barometer.Helpers;
using barometer.Models.Database;
using barometer.Models.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace barometer.Controllers.Api
{
    [Authorize]
    public class BenchmarkingController : ApiController
    {
        private readonly BarometerDbContext context;

        public BenchmarkingController()
        {
            context = new BarometerDbContext();
        }

        [HttpPost]
        public IEnumerable<object> GetBenchmarkingChartData(BenchmarkingViewModel viewModel)
        {
            return BenchmarkingHelper.GetBenchmarkingChartData(context, viewModel, User);
        }
    }
}
