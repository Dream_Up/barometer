﻿using barometer.Helpers;
using barometer.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace barometer.Controllers.Api
{
    [Authorize(Roles = "Operator,Admin")]
    public class StatisticsController : ApiController
    {
        private readonly BarometerDbContext context;

        public StatisticsController()
        {
            context = new BarometerDbContext();
        }

        [HttpGet]
        public object GetStatisticsByPeriod(int periodId)
        {
            return new
            {
                data = StatisticsHelper.GetStatisticsByPeriod(periodId, context)
            };
        }
    }
}
