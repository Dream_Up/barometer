﻿using barometer.Helpers;
using barometer.Models.Database;
using barometer.Models.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace barometer.Controllers.Api
{
    [Authorize]
    public class ReviewOfDataController : ApiController
    {
        private readonly BarometerDbContext context;

        public ReviewOfDataController()
        {
            context = new BarometerDbContext();
        }

        [HttpPost]
        public  IEnumerable<object> GetReviewOfDataChartData(ReviewOfDataViewModel model)
        {
            if (!(model.Star.Count == 1 && model.Star[0] == 5))
            {
                return ReviewOfDataHelper.GetDataChart(context, model);
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
