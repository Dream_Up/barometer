﻿using barometer.Helpers;
using barometer.Models.Database;
using barometer.Models.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace barometer.Controllers.Api
{
    [Authorize]
    public class CommentsController : ApiController
    {
        private readonly BarometerDbContext context;

        public CommentsController()
        {
            context = new BarometerDbContext();
        }

        [Authorize]
        [HttpGet]
        public IEnumerable<string> GetCommentsByPeriod(int periodId)
        {
            var model = CommentsHelper.GetCommentsByPeriod(periodId, context);
            return model;
        }
        
        [HttpGet]
        [Authorize(Roles = "Operator,Admin")]
        public object GetCommentsByPeriods(int periodFirst, int periodSecond)
        {
            return new
            {
                data = CommentsHelper.GetCommentsByPeriods(periodFirst, periodSecond, context)
            };
        }

        [HttpPost]
        [Authorize(Roles = "Operator,Admin")]
        public void SaveComment(EditCommentViewModel model)
        {
            CommentsHelper.SaveComment(model, context, RequestContext.Principal.Identity.Name);
        }

        [HttpPost]
        [Authorize(Roles = "Operator,Admin")]
        public void DeleteComment(int id)
        {
            CommentsHelper.RemoveComment(id, context);
        }

    }
}
