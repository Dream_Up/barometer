﻿using barometer.Helpers;
using barometer.Models.Business;
using barometer.Models.Database;
using barometer.Models.Enums;
using barometer.Models.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;

namespace barometer.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly BarometerDbContext context;

        public HomeController()
        {
            context = new BarometerDbContext();
        }

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                var dateNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                dateNow = DateTime.Now.Day >=10  ? dateNow.AddMonths(-1) : dateNow.AddMonths(-2);
                var model = InfoGraphicsHelper.GeInfoGraphics(context, dateNow, dateNow.AddYears(-1));

                //var model = InfoGraphicsHelper.GeInfoGraphics(context, new DateTime(2016, 6, 1), new DateTime(2015, 6, 1)); For testing

                return View(new HomeIndexViewModel() { InfoGraphicViewModel = model });
            }
            else
            {
                if (User.IsInRole("Operator"))
                {
                    return RedirectToAction("Statistics", "Operators");
                }
                else
                {
                    return RedirectToAction("InsertData", "Home");
                }
            }
        }

        public ActionResult InfoGraphics()
        {
            var model = InfoGraphicsHelper.GetManageInfoGraphicsViewModel(context);
            return View(model);
        }

        [HttpGet]
        public ActionResult GetInfoGraphics(string periodDate, bool isFirstInfoGrahic)
        {
            int year = 0;
            int month = 0;
            if (string.IsNullOrEmpty(periodDate))
            {
                var dateNow = DateTime.Now.AddMonths(-1);
                year = isFirstInfoGrahic ? dateNow.Year : dateNow.Year - 1;
                month = dateNow.Month;
            }
            else
            {
                var arrayParam = periodDate.Split('-');
                year = Convert.ToInt32(arrayParam[1]);
                month = Convert.ToInt32(arrayParam[0]);
            }

            var model = InfoGraphicsHelper.GeInfoGraphics(context, new DateTime(year, month, 1), new DateTime(year - 1, month, 1));

            return PartialView("_InfoGraphics", model);
        }

        [HttpGet]
        public ActionResult ReviewOfData()
        {

            //return View("NotImplement");
            var model = ReviewOfDataHelper.GetReviewOfDataViewModel(context, UserManager.GetRoles(User.Identity.GetUserId())[0]);
            return View(model); 
        }

        [HttpGet]
        public ActionResult Benchmarking()
        {
            var model = BenchmarkingHelper.GetBenchmarkingViewModel(context);
            return View(model);
        }

        [HttpGet]
        public ActionResult Feedback()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "TIC, Hotel, Hostel, Airport")]
        public ActionResult InsertData()
        {
            var model = InsertDataHelper.GetInsertDataViewModel(context, User);
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "TIC, Hotel, Hostel, Airport")]
        public void InsertData(InsertDataSaveModel model)
        {
           InsertDataHelper.Save(model, context, User);
        }

        [HttpGet]
        [Authorize(Roles = "TIC, Hotel, Hostel, Airport")]
        public ActionResult InsertDataPartial(int periodId)
        {
            var model = InsertDataHelper.GetInsertDataPartialViewModel(context, User, periodId);
            return PartialView("Partial/_InsertData", model);
        }
    }
}