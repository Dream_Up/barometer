﻿using barometer.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace barometer.Controllers
{
    [Authorize]
    public class FeedbackController : Controller
    {
        [HttpPost]
        public JsonResult Upload(FeedbackViewModel model)
        {
            MailMessage message = new MailMessage("support@barometer.lviv.ua", "info@barometer.lviv.ua")
            {
                Subject = "barometer.lviv.ua - зворотній зв'язок",
                BodyEncoding = Encoding.UTF8,
                Body = model.ToString() + "User: " + User.Identity.Name,
                IsBodyHtml = true,
                SubjectEncoding = Encoding.UTF8,
            };

            if (model.FileUpload != null)
            {
                var extension = Path.GetExtension(model.FileUpload.FileName).ToLower();
                if (extension != ".jpg" && extension != ".png")
                {
                    return Json(new { Warning = "Допустимі формати png або jpeg! " });
                }

                // Create  the file attachment for this e-mail message.
                Attachment data = new Attachment(model.FileUpload.InputStream, model.FileUpload.FileName);

                message.Attachments.Add(data);
            }
           
            SmtpClient client = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            client.Send(message);

            return Json(new { Success = "Дані успішно відправлені!" });
        }
    }
}