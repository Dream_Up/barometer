﻿using barometer.Helpers;
using barometer.Models.Database;
using barometer.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace barometer.Controllers
{
    [Authorize(Roles="Operator,Admin")]
    public class OperatorsController : Controller
    {
        private readonly BarometerDbContext context;

        public OperatorsController()
        {
            context = new BarometerDbContext();
        }

        public ActionResult Statistics()
        {
            var model = CommonHelper.GetPeriods(context);
            return View(model);
        }

        [HttpGet]
        public ActionResult Comments()
        {
            var model = CommentsHelper.GetCommentsViewModel(context);
            return View(model);
        }

        [HttpGet]
        public ActionResult GetCommentModalView(int? id)
        {
            var model = CommentsHelper.GetEditCommentViewModel(id.GetValueOrDefault(), context, User.Identity.Name, User.IsInRole("Operator"));
            return PartialView("Partial/_EditComment", model);
        }
    }
}