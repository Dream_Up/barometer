﻿$(document).ready(function () {
    barometer.benchmarking.initialize();
});

var barometer = barometer || {};

barometer.benchmarking = {
    
    initialize: function () {
        var self = this;

        $('#Distance').multiselect({
            includeSelectAllOption: true,
            selectAllText: barometer.config.bootstrapMultiselect.selectAllText,
            allSelectedText: barometer.config.bootstrapMultiselect.allSelectedText,
            nSelectedText: barometer.config.bootstrapMultiselect.nSelectedText,
            onChange: function (option, checked) {
                disAllowEmptyInComboBox(option, checked, $('#Distance'));
            },
            onDeselectAll: function () {
                disAllowDeselectAll($('#Distance'));
            }
        });
        $("#Distance").multiselect('selectAll', false);
        $("#Distance").multiselect('updateButtonText');

        $('#Star').multiselect({
            includeSelectAllOption: true,
            selectAllText: barometer.config.bootstrapMultiselect.selectAllText,
            allSelectedText: barometer.config.bootstrapMultiselect.allSelectedText,
            nSelectedText: barometer.config.bootstrapMultiselect.nSelectedText,
            onChange: function (option, checked) {
                disAllowEmptyInComboBox(option, checked, $('#Star'));
            },
            onDeselectAll: function () {
                disAllowDeselectAll($('#Star'));
            }
        });
        $("#Star").multiselect('selectAll', false);
        $("#Star").multiselect('updateButtonText');

        $('#NumberOfRoom').multiselect({
            includeSelectAllOption: true,
            selectAllText: barometer.config.bootstrapMultiselect.selectAllText,
            allSelectedText: barometer.config.bootstrapMultiselect.allSelectedText,
            nSelectedText: barometer.config.bootstrapMultiselect.nSelectedText,
            onChange: function (option, checked) {
                disAllowEmptyInComboBox(option, checked, $('#NumberOfRoom'));
            },
            onDeselectAll: function () {
                disAllowDeselectAll($('#NumberOfRoom'));
            }
        });
        $("#NumberOfRoom").multiselect('selectAll', false);
        $("#NumberOfRoom").multiselect('updateButtonText');

        self.getComments();
        $("#PeriodForComments").on("change", function () {
            self.getComments();
        });

        $("#filterChart").click(function () {
            const firstPeriodStr = $("#PeriodFirst").val();
            const secondPeriodStr = $("#PeriodSecond").val();

            if (parseInt(firstPeriodStr) <= parseInt(secondPeriodStr)) {
                self.getChart();
            }
            else {
                toastr.warning('Не правильно вибрані періоди!');
            }
        });

        google.charts.load('current', { 'packages': ['corechart'] });
        self.getChart();
    },

    getComments: function()
    {
        getCommentsByPeriod($("#PeriodForComments").val(), function (data) {
            $("#comments").html(data);
        });
    },

    getChart: function()
    {
        var self = this;

        google.charts.setOnLoadCallback(function () {

            var titleFirst = $("#BenchmarkingTitleFirst").val();
            var titleSecond = $("#BenchmarkingTitleSecond").val();
            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Період');
            data.addColumn('number', titleFirst);
            data.addColumn({ type: 'string', role: 'tooltip' });
            data.addColumn('number', titleSecond);
            data.addColumn({ type: 'string', role: 'tooltip' });

            var options = {
                title: 'Середня завантаженість у %',
                legend: {position:'top', alignment: 'end'},
                width: 700,
                height: 450,
                chartArea: {
                    left: 50,
                    top: 50,
                    width: "100%",
                    height: "75%"
                },
                hAxis: { title: 'Періоди', titleTextStyle: { color: '#333' } },
                vAxis: { minValue: 0, maxValue: 100 }
            };

            var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
            chart.draw(data, options);

            barometer.chart.loadingOn();

            var responseText = $.ajax({
                type: "POST",
                cache: false,
                data: {
                    PeriodFirst: $("#PeriodFirst").val(),
                    PeriodSecond: $("#PeriodSecond").val(),
                    UserType: $("#UserType").val(),
                    Distance: $("#Distance").val(),
                    Star: $("#Star").val(),
                    IsConferenceHall: $("#IsConferenceHall").is(':checked'),
                    NumberOfRoom: $("#NumberOfRoom").val()
                },
                url: "/Api/Benchmarking/GetBenchmarkingChartData",
                dataType: "json",
                success: function (chartsdata) {

                    for (var i = 0; i < chartsdata.length; i++) {
                        data.addRow([chartsdata[i].PeriodDate, chartsdata[i].WorkloadItem1, titleFirst + ' '+ chartsdata[i].WorkloadItem1 +' %', chartsdata[i].WorkloadItem2, titleSecond +' '+ chartsdata[i].WorkloadItem2 + ' %']);
                    }

                    chart.draw(data, options);

                    barometer.chart.loadingOff();
                },
                error: function () {
                    barometer.chart.loadingOff();
                }
            });

           
        });
    }
}