﻿$(document).ready(function () {
    barometer.editUserData.initialize();
});

var barometer = barometer || {};

barometer.editUserData = {
    
    initialize: function () {
        var self = this;
        self.isEditDataInProcess = false;

        self.initializeMultiselect($('#DistanceId'));
        self.initializeMultiselect($('#Star'));

        $("#NumberOfBeds, #NumberOfRooms, #NumberOfConferencePosition").keydown(onlyNumberForValidateFields);

        $(".caret").css('float', 'right');
        $(".caret").css('margin', '8px 0');

        $.validator.unobtrusive.parse("#EditUserForm");

        $("#SaveUserData").click(function () {
            if (!self.isEditDataInProcess) {
                self.isEditDataInProcess = true;
                self.saveUserData();
            }
        });
    },

    saveUserData: function () {
        var self = this;

        if ($("#EditUserForm").valid()) {
            self.loadingOn();
            var token = $('input[name="__RequestVerificationToken"]', $("#EditUserForm")).val();
            $.ajax({
                url: '/Account/EditUserData',
                data: {
                    __RequestVerificationToken: token,
                    model: {
                        Email: $("#Email").val(),
                        Name: $("#Name").val(),
                        Surname: $("#Surname").val(),
                        Star: $("#Star").val(),
                        NumberOfBeds: $("#NumberOfBeds").val(),
                        NumberOfRooms: $("#NumberOfRooms").val(),
                        DistanceId: $("#DistanceId").val(),
                        NumberOfConferencePosition: $("#NumberOfConferencePosition").val(),
                        PhoneNumber: $("#PhoneNumber").val()
                    }
                },
                cache: false,
                type: 'POST',
                success: function (data) {
                    self.loadingOff();
                    if (data.SuccessMessage) {
                        toastr.success(data.SuccessMessage);
                    }
                    else if (data.ErrorMessage) {
                        toastr.error(data.ErrorMessage);
                    }
                },
                error: function (errorResult) {
                    toastr.error("Виникла помилка, зверніться до адміністратора");
                    self.loadingOff();
                }
            });
        }
        else {
            self.isEditDataInProcess = false;
            toastr.warning("Перевірте правильність заповнення форми!");
        }
    },

    loadingOn: function () {
        $("#loadingDuringSaveUserInformation").show();
    },

    loadingOff: function () {
        var self = this;
        self.isEditDataInProcess = false;
        $("#loadingDuringSaveUserInformation").hide();
    },

    destroyMultiselect: function (select) {
        select.multiselect('destroy');
    },

    initializeMultiselect: function (select, change) {
        select.multiselect({
            buttonWidth: '100%',
            onChange: change,
        });
    },
}