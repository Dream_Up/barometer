﻿var barometer = barometer || {};

barometer.config = {
    typeData : {
        hostel: 1,
        hotelAndHostelAndTIC: 2,
        hotel: 3,
        regularAirport: 4,
        charterAirport: 5
    },

    userType: {
        hotel: "1",
        hostel: "2",
        operator: "3",
        TIC: "4",
        airport: "5",
        admin: "6"
    },

    bootstrapMultiselect: {
        selectAllText: 'Вибрати всі',
        allSelectedText: 'Обрані всі',
        nSelectedText: 'вибрано'
    }
}