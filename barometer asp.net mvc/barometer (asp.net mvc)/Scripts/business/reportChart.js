﻿var barometer = barometer || {};

barometer.reportChart = {

    optionsPieChart : {
        height: 500,
        width: 720,
        legend: {
            position: 'labeled',
        },
        pieStartAngle: 90,
        pieSliceText: 'none',
        selectionMode: 'multiple',
        tooltip: {
            trigger: 'selection',
            ignoreBounds: true
        },

        chartArea: {
            left: 50,
            top: 50,
            width: "95%",
            height: "85%"
        }
    },

    optionsColumnChart : {
        legend: { position: 'top', alignment: 'end' },
        width: 740,
        height: 450,
        chartArea: {
            left: 50,
            top: 50,
            width: "100%",
            height: "75%"
        }
    },

    optionsTableChart : {
        showRowNumber: false,
        width: 300,
        allowHtml: true,
        cssClassNames: {
            'headerRow': 'cssHeaderRow',
            'tableRow': 'cssTableRow',
            'oddTableRow': 'cssOddTableRow',
            'selectedTableRow': 'cssSelectedTableRow',
            'hoverTableRow': 'cssHoverTableRow',
            'headerCell': 'cssHeaderCell',
            'tableCell': 'cssTableCell',
            'rowNumberCell': 'cssRowNumberCell'
    }},

    getChartGeographyOfVisitors: function (userId, isForCountry, divPie, divTable) {
        var self = this;

        google.charts.setOnLoadCallback(function () {

            var dataChart = new google.visualization.DataTable();
            var dataTable = new google.visualization.DataTable();

            dataTable.addColumn('string', '');
            if (isForCountry) {
                dataChart.addColumn('string', 'Країна');
                dataTable.addColumn('string', 'Країна');
            }
            else {
                dataChart.addColumn('string', 'Місто/область');
                dataTable.addColumn('string', 'Місто/область');
            }

            dataChart.addColumn('number', 'К-сть');
            dataTable.addColumn('number', 'К-сть');

            var chart = new google.visualization.PieChart(document.getElementById(divPie));
            var table = new google.visualization.Table(document.getElementById(divTable));

            var responseText = $.ajax({
                type: "POST",
                cache: false,
                data: {
                    from: $("#fromDate").val(),
                    to: $("#toDate").val(),
                    userId: userId,
                    isForCountry: isForCountry
                },
                url: "/TICData/GetGeographyOfVisitors",
                dataType: "json",
                success: function (chartsdata) {
                    for (var i = 0; i < chartsdata.Pie.length; i++) {
                        if (i !== chartsdata.Pie.length - 1) {
                            dataChart.addRow([chartsdata.Pie[i].Name, chartsdata.Pie[i].Tourist]);
                            dataTable.addRow(['<div class="circle" style="background-color:' + barometer.chart.defaultColors[i] + '!important"></div>', chartsdata.Pie[i].Name, chartsdata.Pie[i].Tourist]);
                        }
                        else {
                            dataTable.addRow(['', chartsdata.Pie[i].Name, chartsdata.Pie[i].Tourist]);
                        }
                    }
                    var blockId;
                    if (userId == null) {
                        blockId = isForCountry ? "page_country" : "page_city";
                    }
                    else {
                        blockId = isForCountry ? "page_country_" + userId : "page_city_" + userId;
                    }
                    self.drawTable(chartsdata.Table, blockId, isForCountry, chartsdata.TotalinP, chartsdata.TotalOther, chartsdata.Previous);

                    google.visualization.events.addListener(chart, 'ready', function () {
                        var svg = jQuery('#' + divPie + ' svg');
                        svg.attr("xmlns", "http://www.w3.org/2000/svg");
                        svg.css('overflow', 'visible');;
                    });

                    google.visualization.events.addListener(table, 'ready', function () {
                        var svg = jQuery('#tableGeographyOfVisitorsCountry svg');
                        svg.attr("xmlns", "http://www.w3.org/2000/svg");
                        svg.css('overflow', 'visible');
                    });

                    chart.draw(dataChart, self.optionsPieChart);
                    table.draw(dataTable, self.optionsTableChart);
                    barometer.chart.loadingReportOff(divPie);
                },
                error: function () {
                    barometer.chart.loadingReportOff(divPie);
                }
            });
        });
    },

    getChartInterestsOfVisitors: function (userId, divPie, divTable) {
        var self = this;

        google.charts.setOnLoadCallback(function () {

            var dataChart = new google.visualization.DataTable();
            var dataTable = new google.visualization.DataTable();

            dataChart.addColumn('string', 'Тематика');
            dataChart.addColumn('number', 'К-сть');
            dataTable.addColumn('string', '');
            dataTable.addColumn('string', 'Тематика');
            dataTable.addColumn('number', 'К-сть');

            var chart = new google.visualization.PieChart(document.getElementById(divPie));
            var table = new google.visualization.Table(document.getElementById(divTable));

            var responseText = $.ajax({
                type: "POST",
                cache: false,
                data: {
                    from: $("#fromDate").val(),
                    to: $("#toDate").val(),
                    userId: userId,
                },
                url: "/TICData/GetInterestsOfVisitors",
                dataType: "json",
                success: function (chartsdata) {
                    for (var i = 0; i < chartsdata.length; i++) {
                        if (i !== chartsdata.length - 1) {
                            dataChart.addRow([chartsdata[i].Name, chartsdata[i].Tourist]);
                            dataTable.addRow(['<div class="circle" style="background-color:' + barometer.chart.defaultColors[i] + '!important"></div>', chartsdata[i].Name, chartsdata[i].Tourist]);
                        }
                        else {
                            dataTable.addRow(['', chartsdata[i].Name, chartsdata[i].Tourist]);
                        }
                    }

                    google.visualization.events.addListener(chart, 'ready', function () {
                        var svg = jQuery('#chartGeographyOfVisitorsCountry svg');
                        svg.attr("xmlns", "http://www.w3.org/2000/svg");
                        svg.css('overflow', 'visible');
                    });

                    chart.draw(dataChart, self.optionsPieChart);
                    table.draw(dataTable, self.optionsTableChart);
                    barometer.chart.loadingReportOff(divPie);
                },
                error: function () {
                    barometer.chart.loadingReportOff(divPie);
                }
            });
        });
    },

    getChartComparisonOfVisitors: function (userId, isForCountry, divColumn, divTable) {
        var self = this;

        google.charts.setOnLoadCallback(function () {

            var dataChart = new google.visualization.DataTable();
            var dataTable = new google.visualization.DataTable();
            var formatter = new google.visualization.NumberFormat({ pattern: '#.#' });

            if (isForCountry) {
                dataChart.addColumn('string', 'Країна');
                dataTable.addColumn('string', 'Країна');
            }
            else {
                dataChart.addColumn('string', 'Місто/область');
                dataTable.addColumn('string', 'Місто/область');
            }

            var chart = new google.visualization.ColumnChart(document.getElementById(divColumn));
            var table = new google.visualization.Table(document.getElementById(divTable));

            var responseText = $.ajax({
                type: "POST",
                cache: false,
                data: {
                    from: $("#fromDate").val(),
                    to: $("#toDate").val(),
                    userId: userId,
                    isForCountry: isForCountry
                },
                url: "/TICData/GetComparisonOfVistors",
                dataType: "json",
                success: function (chartsdata) {

                    dataChart.addColumn('number', chartsdata.Column1);
                    dataChart.addColumn('number', chartsdata.Column2);
                    dataTable.addColumn('number', chartsdata.Column1);
                    dataTable.addColumn('number', chartsdata.Column2);
                    dataTable.addColumn('number', 'Приріст, %');

                    for (var i = 0; i < chartsdata.ColumnChart.length; i++) {
                        if (i !== chartsdata.ColumnChart.length - 1) {
                            dataChart.addRow([chartsdata.ColumnChart[i].Name, chartsdata.ColumnChart[i].TouristsNow, chartsdata.ColumnChart[i].TouristsPrev]);
                        }
                        dataTable.addRow([chartsdata.ColumnChart[i].Name, chartsdata.ColumnChart[i].TouristsNow, chartsdata.ColumnChart[i].TouristsPrev, chartsdata.ColumnChart[i].Increase]);
                    }

                    formatter.format(dataTable, 1);
                    formatter.format(dataTable, 2);
                    formatter.format(dataTable, 3);
                    formatter.format(dataChart, 1);
                    formatter.format(dataChart, 2);

                    chart.draw(dataChart, self.optionsColumnChart);
                    table.draw(dataTable, self.optionsTableChart);
                    barometer.chart.loadingReportOff(divColumn);
                },
                error: function () {
                    barometer.chart.loadingReportOff(divColumn);
                }
            });
        });
    },
    getChartComparisonOfVisitorsTEST: function (userId, isForCountry, divColumn, divTable) {
        var self = this;

        google.charts.setOnLoadCallback(function () {
            var dataChart = new google.visualization.DataTable();

            dataChart.addColumn('timeofday', 'Time of Day');
            dataChart.addColumn('number', 'Motivation Level');
            dataChart.addColumn('number', 'Energy Level');

            dataChart.addRows([
                [{ v: [8, 0, 0], f: '8 am' }, 1, .25],
                [{ v: [9, 0, 0], f: '9 am' }, 2, .5],
                [{ v: [10, 0, 0], f: '10 am' }, 3, 1],
                [{ v: [11, 0, 0], f: '11 am' }, 4, 2.25],
                [{ v: [12, 0, 0], f: '12 pm' }, 5, 2.25],
                [{ v: [13, 0, 0], f: '1 pm' }, 6, 3],
                [{ v: [14, 0, 0], f: '2 pm' }, 7, 4],
                [{ v: [15, 0, 0], f: '3 pm' }, 8, 5.25],
                [{ v: [16, 0, 0], f: '4 pm' }, 9, 7.5],
                [{ v: [17, 0, 0], f: '5 pm' }, 10, 10],
            ]);


            var chart = new google.visualization.ColumnChart(document.getElementById(divColumn));
            //var table = new google.visualization.Table(document.getElementById(divTable));

            chart.draw(dataChart, self.optionsColumnChart);
            //table.draw(dataTable, self.optionsTableChart);
            barometer.chart.loadingReportOff(divColumn);
        });
    },
    drawTable: function (data, id, isForCountry, totalinP, total, previous) {
        var column1 = isForCountry ? "Країна" : 'Місто/область';
        var html =
            "<div class='clearfix'></div>" +
            "<div class='page'>" +
            "<img src='" + $("#Image").val() + "' width='140' height='847' alt='' class='pull-left' />" +
            "<div class='left-logo pull-left'>" +
                "<img src='" + $("#ReportLogo").val() + "' width='39' height='57' alt='' class='pull-left'>" +
                "<p class='pull-left'>Центр туристичної інформації</p>" +
            "</div>" +
            "<div class='table-for-report pull-left'>" +
            "<h6>" + $("#fromDate").val() + " - " + $("#toDate").val() + "</h6>" +
            "<h3>Інші " + (isForCountry ? "країни" : "міста") + " " + totalinP + "%</h3>" +
            "<h4>(менше " + previous + ", загалом " + total + ")</h4>" +
            "<div class='table-for-report-header'>" +
            "<div class='row-report'>" +
                "<div class='pull-left cell-name'>" + column1 + "</div>" +
                "<div class='pull-left cell-value'>К-сть</div>" +
            "</div>" +
            "<div class='row-report'>" +
                "<div class='pull-left cell-name'>" + column1 + "</div>" +
                "<div class='pull-left cell-value'>К-сть</div>" +
            "</div>" +
            "<div class='row-report'>" +
                "<div class='pull-left cell-name'>" + column1 + "</div>" +
                "<div class='pull-left cell-value'>К-сть</div>" +
            "</div>" +
            "</div>" +
            "<div class='table-for-report-body'>";
        $.each(data, function (key, value) {
            html += "<div class='row-report'><div class='pull-left cell-name'>" + value.Name +
                "</div><div class='pull-left cell-value'>" + value.Tourist +
                "</div><div class='clearfix'></div></div>";
        });

        var length = data.length;
        while (length % 3 != 0) {
            html += "<div class='row-report'>" +
                        "<div class='pull-left cell-name'></div>" +
                        "<div class='pull-left cell-value'></div>" +
                        "<div class='clearfix'></div>" +
                    "</div>";
            length++;
        }

        html += "</div>" +
            "<div class='table-for-report-footer'>" +
                "<div class='pull-left cell-name'>Загалом</div>" +
                "<div class='pull-left cell-value'>" + total + "</div>" +
                 "<div class='clearfix'></div>" +
            "</div>" +
        "</div>" +
        "<div class='clearfix'></div>" +
        "</div>";
        $(html).insertAfter("#" + id);
    }
}