﻿$(document).ready(function () {
    barometer.insertData.initialize();
});

var barometer = barometer || {};

barometer.insertData = {
    
    initialize: function () {
        var self = this;
        this.isSaveInProcess = false;

        self.getInserDataPartial($('.period-block-active').attr('data-period-id'));

        $('div.period-block').click(function () {
            $('.period-block-active').removeClass('period-block-active');
            $(this).addClass('period-block-active');
            self.getInserDataPartial($('.period-block-active').attr('data-period-id'));
        });
    },

    getInserDataPartial: function (periodId) {
        var self = this;
        $("#insertDataPartial").html('<div class="loading-insertData"></div>');
        $.ajax({
            url: '/Home/InsertDataPartial?periodId=' + periodId,
            dataType: 'html',
            cache: false,
            type: 'GET',
            success: function (data) {
                $("#insertDataPartial").html(data);
                $("div[data-period-id='" + periodId + "'] .period-block-count").text($("#CountTourists").val())
                
                $("#saveInsertData").click(function () {
                    if (!self.isSaveInProcess) {
                        self.isSaveInProcess = true;
                        self.saveInsertData();
                    }
                });
            }
        });
    },

    saveInsertData: function () {
        var self = this;
        var data = [];
        var sum = 0;
        var fieldValue;
        var warningMessages = [];
        $('#loading-during-saving').show();
        $.each($('#insertDataForm input'), function (key, element) {
            fieldValue = $(element).val();
            sum += (fieldValue != null) ? fieldValue : 0;
            data.push({ key: $(element).prop('id'), value: { Id: $(element).attr('data-id-val'), Value: fieldValue, TypeData: $(element).attr('data-type-val') } });
        });

        if (sum == 0)
        {
            warningMessages.push('Загальна сума не повинна дорівнювати нулю!!!');
        }

        if (warningMessages.length == 0) {
            $.ajax({
                url: '/Home/InsertData',
                cache: false,
                type: 'POST',
                data: JSON.stringify({
                    Data: data,
                    PeriodId: $('.period-block-active').attr('data-period-id'),
                    CommentText: $('#Comment_Text').val(),
                    CommentId: $('#Comment_Id').val()
                }),
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    self._hideLoadingAfterSave();
                    toastr.success('Дані успішно збережені!');
                },
                error: function () {
                    self._hideLoadingAfterSave();
                    toastr.error('Виникла помилка! Перезавантажте сторінку і спробуйте ще раз.');
                }
            });
        }
        else {
            self._hideLoadingAfterSave();
            for (var i = 0; i < warningMessages.length; i++)
            {
                toastr.warning(warningMessages[i]);
            }
        }
    },

    _hideLoadingAfterSave: function () {
        var self = this;
        self.getInserDataPartial($('.period-block-active').attr('data-period-id'));
        self.isSaveInProcess = false;
        $('#loading-during-saving').hide();
    }
}