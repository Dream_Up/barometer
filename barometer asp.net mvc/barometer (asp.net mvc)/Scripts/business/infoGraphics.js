﻿$(document).ready(function () {
    barometer.infoGraphics.initialize();
});

var barometer = barometer || {};

barometer.infoGraphics = {
    
    initialize: function () {
        var self = this;

        self.loading = "<div class='loading-infoGraphics'></div>"

        self.getInfoGraphic('', true, "#infoGraphics-first-block");
        self.getInfoGraphic('', false, "#infoGraphics-second-block");

        $("#PeriodFirst").on("change", function () {
            $("#infoGraphics-first-block").html(self.loading);
            self.getInfoGraphic($("#PeriodFirst option:selected").text(), true, "#infoGraphics-first-block");
        });

        $("#PeriodSecond").on("change", function () {
            $("#infoGraphics-second-block").html(self.loading);
            self.getInfoGraphic($("#PeriodSecond option:selected").text(), false, "#infoGraphics-second-block");
        });
    },

    getInfoGraphic: function (date, isFirst, block)
    {
        $.ajax({
            url: '/Home/GetInfoGraphics?periodDate=' + date + '&isFirstInfoGrahic='+isFirst,
            dataType: 'html',
            cache: false,
            type: 'GET',
            success: function (data) {
                $(block).html(data);
            }
        });
    }
};