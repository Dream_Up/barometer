﻿$(document).ready(function () {
    barometer.register.initialize();
});

var barometer = barometer || {};

barometer.register = {
    
    initialize: function () {
        var self = this;
        self.isRegisterInProcess = false;

        self.initializeMultiselect($('#RoleName'), self.changeUserRole);
        self.initializeMultiselect($('#DistanceId'));
        self.initializeMultiselect($('#Star'));

        $(".caret").css('float', 'right');
        $(".caret").css('margin', '8px 0');

        $.validator.unobtrusive.parse("#RegisterForm");

        $("#registerNewUser").click(function () {
            if (!self.isRegisterInProcess) {
                self.isRegisterInProcess = true;
                self.saveNewUser();
            }
        });

        $("#NumberOfBeds, #NumberOfRooms, #NumberOfConferencePosition").keydown(onlyNumberForValidateFields);
    },

    saveNewUser: function () {
        var self = this;

        if ($("#RegisterForm").valid()) {
            self.loadingOn();
            var token = $('input[name="__RequestVerificationToken"]', $("#RegisterForm")).val();
            $.ajax({
                url: '/Account/Register',
                data: {
                    __RequestVerificationToken: token,
                    model: {
                        UserName: $("#UserName").val(),
                        Email: $("#Email").val(),
                        Password: $("#Password").val(),
                        Name: $("#Name").val(),
                        Surname: $("#Surname").val(),
                        RoleName: $("#RoleName").val(),
                        Star: $("#Star").val(),
                        NumberOfBeds: $("#NumberOfBeds").val(),
                        NumberOfRooms: $("#NumberOfRooms").val(),
                        DistanceId: $("#DistanceId").val(),
                        NumberOfConferencePosition: $("#NumberOfConferencePosition").val(),
                        PhoneNumber: $("#PhoneNumber").val()
                    }
                },
                cache: false,
                type: 'POST',
                success: function (data) {
                    self.loadingOff();
                    if (data.SuccessMessage) {
                        toastr.success(data.SuccessMessage);
                        $("#RegisterForm input").not("#NumberOfBeds, #NumberOfRooms, #NumberOfConferencePosition, [name='__RequestVerificationToken']").val(null);

                        var roleName = $("#RoleName").val();

                        $("#RoleName").val(roleName);
                        self.destroyMultiselect($("#RoleName"));
                        self.initializeMultiselect($("#RoleName"), self.changeUserRole);

                        if (roleName == "Hotel" || roleName == "Hostel")
                        {
                            $('#DistanceId').val($('#DistanceId').find('option').val());
                            self.destroyMultiselect($('#DistanceId'));
                            self.initializeMultiselect($('#DistanceId'));
                            $("#NumberOfBeds").val(0);
                        }

                        if (roleName == "Hotel") {
                            $('#Star').val($('#Star').find('option').val());
                            self.destroyMultiselect($('#Star'));
                            self.initializeMultiselect($('#Star'));
                            $("#NumberOfRooms").val(0);
                            $("#NumberOfConferencePosition").val(0);
                        }

                        $(".caret").css('float', 'right');
                        $(".caret").css('margin', '8px 0');
                    }
                    else if (data.ValidationMessages) {
                        $.each(data.ValidationMessages, function (key, value) {
                            $("[data-valmsg-for="+key+"]", $("#RegisterForm")).html(value);
                        });
                        toastr.warning("Форма не валідна!");
                    }
                    else if (data.ErrorMessage) {
                        toastr.error(data.ErrorMessage);
                    }
                },
                error: function (errorResult) {
                    toastr.error("Виникла помилка, зверніться до адміністратора");
                    self.loadingOff();
                }
            });
        }
        else {
            self.isRegisterInProcess = false;
            toastr.warning("Форма не валідна!");
        }
    },

    loadingOn: function () {
        $("#loadingDuringRegister").show();
    },

    loadingOff: function () {
        var self = this;
        self.isRegisterInProcess = false;
        $("#loadingDuringRegister").hide();
    },

    destroyMultiselect: function (select) {
        select.multiselect('destroy');
    },

    initializeMultiselect: function (select, change)
    {
        select.multiselect({
            buttonWidth: '100%',
            onChange: change, 
        });
    },

    changeUserRole: function () {
        var self = this;
        switch ($('#RoleName').val()) {
            case 'Hotel':   
                barometer.register.showControl("NumberOfRooms");
                barometer.register.showControl("NumberOfConferencePosition");
                barometer.register.showControl("NumberOfBeds");
                barometer.register.showControl("Star");
                barometer.register.showControl("DistanceId");

                $('#Star').val($('#Star').find('option').val());
                barometer.register.destroyMultiselect($('#Star'));
                barometer.register.initializeMultiselect($('#Star'));

                $('#DistanceId').val($('#DistanceId').find('option').val());
                barometer.register.destroyMultiselect($('#DistanceId'));
                barometer.register.initializeMultiselect($('#DistanceId'));
            break;
            case 'Hostel': 
                barometer.register.hideControl("NumberOfRooms");
                barometer.register.hideControl("NumberOfConferencePosition");
                barometer.register.showControl("NumberOfBeds");
                barometer.register.hideControl("Star");
                barometer.register.showControl("DistanceId");

                $('#DistanceId').val($('#DistanceId').find('option').val());
                barometer.register.destroyMultiselect($('#DistanceId'));
                barometer.register.initializeMultiselect($('#DistanceId'));
            break;
            case 'TIC':
                barometer.register.hideControl("NumberOfRooms");
                barometer.register.hideControl("NumberOfConferencePosition");
                barometer.register.hideControl("NumberOfBeds");
                barometer.register.hideControl("Star");
                barometer.register.showControl("DistanceId");

                $('#DistanceId').val($('#DistanceId').find('option').val());
                barometer.register.destroyMultiselect($('#DistanceId'));
                barometer.register.initializeMultiselect($('#DistanceId'));
            break;
            case 'Airport':
                barometer.register.hideControl("NumberOfRooms");
                barometer.register.hideControl("NumberOfConferencePosition");
                barometer.register.hideControl("NumberOfBeds");
                barometer.register.hideControl("Star");
                barometer.register.showControl("DistanceId");

                $('#DistanceId').val($('#DistanceId').find('option').val());
                barometer.register.destroyMultiselect($('#DistanceId'));
                barometer.register.initializeMultiselect($('#DistanceId'));
            break;
            case 'Operator':
                barometer.register.hideControl("NumberOfRooms");
                barometer.register.hideControl("NumberOfConferencePosition");
                barometer.register.hideControl("NumberOfBeds");
                barometer.register.hideControl("Star");
                barometer.register.hideControl("DistanceId");
            break;
        }
    },

    hideControl: function (selector) {
        $("." + selector).hide();
        $("#" + selector).val(0);
    },

    showControl: function (selector) {
        $("." + selector).show();
        $("#" + selector).val(0);
    }
}