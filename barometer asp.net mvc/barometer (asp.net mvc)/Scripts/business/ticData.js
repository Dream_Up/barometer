﻿$(document).ready(function () {
    barometer.ticData.initialize();
});

var barometer = barometer || {};

barometer.ticData = {
    
    initialize: function () {
        var self = this;
        self.isEditDataInProcess = false;

        $("#dateFrom, #dateTo").datetimepicker({
            format: "DD.MM.YYYY",
            defaultDate: new Date()
        });

        $("#createIndividualTICReport").click(function () {
            if (self.validateDate()) {
                var win = window.open('/TICData/IndividualReport?from=' + $("#dateFrom").data('date') + '&to=' + $("#dateTo").data('date'), '_blank');
                if (win) {
                    win.focus();
                }
            }
        });

        $("#createTICReport").click(function () {
            if (self.validateDate()) {
                var win = window.open('/TICData/Report?from=' + $("#dateFrom").data('date') + '&to=' + $("#dateTo").data('date'), '_blank');
                if (win) {
                    win.focus();
                }
            }
        });

        $("#analyticsReport").click(function () {
            if (self.validateDate()) {
                var win = window.open('/TICData/AnalyticsReport?from=' + $("#dateFrom").data('date') + '&to=' + $("#dateTo").data('date'), '_blank');
                if (win) {
                    win.focus();
                }
            }
        });

        $('#btnUpload').click(function () {  
  
            if (window.FormData !== undefined) {
                if ($("#FileUpload1").val() && $("#FileUpload1").val() != "") {
                    self.loadingOn();

                    var fileUpload = $("#FileUpload1").get(0);
                    var files = fileUpload.files;
                    var fileData = new FormData();

                    for (var i = 0; i < files.length; i++) {
                        fileData.append(files[i].name, files[i]);
                    }

                    if ($(".ticData-import-log-visible").is(":hidden")) {
                        $(".ticData-import-log-visible").show();
                    }
                    self.manageLog(null, moment().format('DD.MM.YYYY hh:mm:ss') + ' Імпорт файлу - "' + $("#FileUpload1").val() + '"', "green");

                    $.ajax({
                        url: '/TICData/UploadReport',
                        type: "POST",
                        contentType: false,
                        processData: false,
                        data: fileData,
                        success: function (result) {
                            self.loadingOff();
                            if (result.SuccessMessage) {
                                toastr.success(result.SuccessMessage);
                                self.manageLog(result.ListNotExsist, moment().format('DD.MM.YYYY hh:mm:ss') + " Файл успішно імпортованно!", "green");
                            }
                            else if (result.ErrorMessage) {
                                toastr.error(result.ErrorMessage);
                                self.manageLog(null, moment().format('DD.MM.YYYY hh:mm:ss') + ' ' + result.ErrorMessage, "red");
                            }
                            else if (result.WarningMessage) {
                                toastr.warning(result.WarningMessage);
                                self.manageLog(null, moment().format('DD.MM.YYYY hh:mm:ss') + ' ' + result.WarningMessage, "orange");
                            }
                        },
                        error: function (err) {
                            self.loadingOff();
                            toastr.error(err.statusText);
                            self.manageLog(null, moment().format('DD.MM.YYYY hh:mm:ss') + ' ' + rerr.statusText, "red");
                        }
                    });
                }
                else {
                    toastr.warning("Файл не вибрано!");
                }
            } else {  
                toastr.error("Завантаження не можливе!");  
            }  
        });  
    },

    validateDate: function () {
        var from = moment($("#dateFrom").data('date'), "DD.MM.YYYY", true);
        var to = moment($("#dateTo").data('date'), "DD.MM.YYYY", true);
        if (from.isValid() && to.isValid()) {
            if (from.toDate() > to.toDate())
            {
                toastr.warning("Дата 'Від' повинна бути більша або дорівнювати даті 'До' !");
                return false;
            }
            else if (from.year() != to.year())
            {
                toastr.warning("Оберіть дати в межах одного року!");
                return false;
            }
            return true;
        }
        else {
            toastr.warning("Дата повинна бути у форматі ДД.ММ.РРРР !");
            return false;
        }
    },

    manageLog: function(logs, message, color)
    {
        if (logs && logs.length > 0) {

            $.each(logs, function (key, value) {
                $("#ticDataImportLog").prepend('<p style="color:red">*** Рядок №' + value.Item1 + ' Країна/область/місто - "' + value.Item2 + '" не був імпортований!</p>');
            });
        }

        if (message) {
            $("#ticDataImportLog").prepend('<p style="color:' + color + '">*** ' + message + '</p>');
        }
    },

    loadingOn: function () {
        $("#loadingDuringParseXSLS").show();
    },

    loadingOff: function () {
        var self = this;
        self.isEditDataInProcess = false;
        $("#loadingDuringParseXSLS").hide();
    },
}