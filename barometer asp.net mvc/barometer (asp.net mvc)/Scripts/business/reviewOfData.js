﻿$(document).ready(function () {
    barometer.reviewOfData.initialize();
});

var barometer = barometer || {};

barometer.reviewOfData = {
    
    initialize: function () {
        var self = this;

        self.chageUserType();
        $('#UserType').multiselect({
            nonSelectedText: 'Тип невибраний',
            onChange: function (option, checked) {
                self.chageUserType();
            }
        });
        $('#Distance').multiselect({
            includeSelectAllOption: true,
            selectAllText: barometer.config.bootstrapMultiselect.selectAllText,
            allSelectedText: barometer.config.bootstrapMultiselect.allSelectedText,
            nSelectedText: barometer.config.bootstrapMultiselect.nSelectedText,
            onChange: function (option, checked) {
                disAllowEmptyInComboBox(option, checked, $('#Distance'));
            },
            onDeselectAll: function () {
                disAllowDeselectAll($('#Distance'));
            }
        });
        $("#Distance").multiselect('selectAll', false);
        $("#Distance").multiselect('updateButtonText');

        $('#Star').multiselect({
            includeSelectAllOption: true,
            selectAllText: barometer.config.bootstrapMultiselect.selectAllText,
            allSelectedText: barometer.config.bootstrapMultiselect.allSelectedText,
            nSelectedText: barometer.config.bootstrapMultiselect.nSelectedText,
            onChange: function (option, checked) {
                disAllowEmptyInComboBox(option, checked, $('#Star'));
            },
            onDeselectAll: function () {
                disAllowDeselectAll($('#Star'));
            }
        });
        $("#Star").multiselect('selectAll', false);
        $("#Star").multiselect('updateButtonText');

        $('#NumberOfRoom').multiselect({
            includeSelectAllOption: true,
            selectAllText: barometer.config.bootstrapMultiselect.selectAllText,
            allSelectedText: barometer.config.bootstrapMultiselect.allSelectedText,
            nSelectedText: barometer.config.bootstrapMultiselect.nSelectedText,
            onChange: function (option, checked) {
                disAllowEmptyInComboBox(option, checked, $('#NumberOfRoom'));
            },
            onDeselectAll: function () {
                disAllowDeselectAll($('#NumberOfRoom'));
            }
        });
        $("#NumberOfRoom").multiselect('selectAll', false);
        $("#NumberOfRoom").multiselect('updateButtonText');

        $('#Country').multiselect({
            includeSelectAllOption: true,
            selectAllText: barometer.config.bootstrapMultiselect.selectAllText,
            allSelectedText: barometer.config.bootstrapMultiselect.allSelectedText,
            nSelectedText: barometer.config.bootstrapMultiselect.nSelectedText,
            onChange: function (option, checked) {
                disAllowEmptyInComboBox(option, checked, $('#Country'));
            },
            onDeselectAll: function () {
                disAllowDeselectAll($('#Country'));
            }
        });
        $("#Country").multiselect('selectAll', false);
        $("#Country").multiselect('updateButtonText');

        google.charts.load('current', { 'packages': ['corechart'] });
        self.getChart();

        self.getComments();
        $("#PeriodForComments").on("change", function () {
            self.getComments();
        });

        self.getGrids();
        $("#filterTable").click(function () {
            const firstPeriodStr = $("#PeriodFirst").val();
            const secondPeriodStr = $("#PeriodSecond").val();

            if (parseInt(firstPeriodStr) <= parseInt(secondPeriodStr)) {
                self.reviewOfDataTableFirst.destroy();
                $("#reviewOfDataTableFirst tbody, #reviewOfDataTableFirst thead tr, #reviewOfDataTableFirst tfoot tr").empty();
                if (self.reviewOfDataTableSecond) {
                    self.reviewOfDataTableSecond.destroy();
                    $("#reviewOfDataTableSecond tbody, #reviewOfDataTableSecond thead tr, #reviewOfDataTableSecond tfoot tr").empty();
                }
                self.getGrids();
                self.getChart();
            }
            else {
                toastr.warning('Не правильно вибрані періоди!');
            }
        });

        self.isSaveInProcess = false;
        $("#reviewOfDataReport #createReport").click(function () {
            if (!self.isSaveInProcess) {
                self.isSaveInProcess = true;
                self.reportLoadingOn();
                self.getReport();
            }
            return false;
        });

        $('#reviewOfDataTableFirst_filter').hide();
    },

    getComments: function()
    {
        getCommentsByPeriod($("#PeriodForComments").val(), function (data) {
            $("#comments").html(data);
        })
    },

    getGrids: function () {
        var self = this;
        self.getTableColumn(function (data) {
            self.reviewOfDataTableFirst = self.getReviewOfDataTable("#reviewOfDataTableFirst", data, true);
            if ($("#UserType").val() !== barometer.config.userType.TIC) {
                $("#tableSecondGroup").show();
                self.reviewOfDataTableSecond = self.getReviewOfDataTable("#reviewOfDataTableSecond", data, false);
            }
            else {
                self.reviewOfDataTableSecond = undefined;
                $("#tableSecondGroup").hide();
            }
        });
    },

    getReviewOfDataTable: function (tableId, data, isFirst)
    {
        var self = this;
        $(tableId + ' thead tr').html(data.Item1);

        var columns = JSON.parse(data.Item2);

        return $(tableId)
        .on('preXhr.dt', function (e, settings, data) {
            $(tableId + ' tbody').html('<tr><td colspan="'+$(tableId +' th').length+'" class="loading"></td></tr>');
            $("input, select, button").not("input[type='hidden'], #PeriodForComments, .not-hidden").prop("disabled", true);
        })
        .on('xhr.dt', function (e, settings, json, xhr) {
            $("input, select, button").not("input[type='hidden'], #PeriodForComments, .not-hidden").prop("disabled", false);
        }).DataTable({
            scrollY: "500px",
            scrollCollapse: true,
            paging: false,
            info: false,
            scrollX: ($("#PeriodSecond").val() - $("#PeriodFirst").val() > 9) ? true : false,
            order: [[columns.length - 1, "desc"]],
            initComplete: function () {
                var api = this.api();
                htmlRow = "<th>Сума</th>"
                $.each(api.columns()[0], function (key, value) {
                    if (value !== 0) {
                        htmlRow += "<th>" + api.column(value).data().sum() + "</th>";

                    }
                });
                $(api.table().footer()).find("tr").html(htmlRow);
                api.table().columns.adjust().draw();
            },
            language: {
                loadingRecords: "Завантаження...",
                emptyTable: "За даними критеріями нічого не знайдено...",
                zeroRecords: "За даними критеріями нічого не знайдено...",
                search: "Пошук: "
            },
            ajax: {
                url: "/ReviewOfData/GetTableData",
                cache: false,
                type: 'POST',
                data: {
                    model: {
                        PeriodFirst: $("#PeriodFirst").val(),
                        PeriodSecond: $("#PeriodSecond").val(),
                        UserType: $("#UserType").val(),
                        Distance: $("#Distance").val(),
                        Star: $("#Star").val(),
                        IsFirst: isFirst,
                        IsConferenceHall: $("#IsConferenceHall").is(':checked'),
                        NumberOfRoom: $("#NumberOfRoom").val(),
                        Country: $("#Country").val()
                    }
                }
            },
            columns: columns
        });
    },

    getTableColumn: function(callback)
    {
        $.ajax({
            url: '/ReviewOfData/GetTableColumn?periodFirst=' + $("#PeriodFirst").val() + '&periodSecond=' + $("#PeriodSecond").val(),
            dataType: 'json',
            cache: false,
            type: 'GET',
            success: callback
        });
    },

    getChart: function()
    {
        var self = this;

        google.charts.setOnLoadCallback(function () {

            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Період');

            var userType = $("#UserType").val();
            switch(userType)
            {
                case barometer.config.userType.TIC:
                    data.addColumn('number', "Туристів");
                    break;
                case barometer.config.userType.airport:
                    data.addColumn('number', "Регулярні рейси");
                    data.addColumn('number', "Чартерні рейси");
                    break;
                default:
                    data.addColumn('number', "Туристів");
                    data.addColumn('number', "Ліжко-ночей");
            }

            var options = {
                title: '',
                legend: { position: 'top', alignment: 'end' },
                width: 700,
                height: 450,
                chartArea: {
                    left: 50,
                    top: 50,
                    width: "100%",
                    height: "75%"
                },
                hAxis: { title: 'Періоди', titleTextStyle: { color: '#333' } },
                vAxis: { minValue: 0}
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            chart.draw(data, options);

            barometer.chart.loadingOn();

            var responseText = $.ajax({
                type: "POST",
                cache: false,
                data: {
                    PeriodFirst: $("#PeriodFirst").val(),
                    PeriodSecond: $("#PeriodSecond").val(),
                    UserType: $("#UserType").val(),
                    Distance: $("#Distance").val(),
                    Country: $("#Country").val(),
                    Star: $("#Star").val(),
                    IsConferenceHall: $("#IsConferenceHall").is(':checked'),
                    NumberOfRoom: $("#NumberOfRoom").val()
                },
                url: "/Api/ReviewOfData/GetReviewOfDataChartData",
                dataType: "json",
                success: function (chartsdata) {

                    for (var i = 0; i < chartsdata.length; i++) {
                        if (userType != barometer.config.userType.TIC) {
                            data.addRow([chartsdata[i].PeriodDate, chartsdata[i].SumFirst, chartsdata[i].SumSecond]);
                        }
                        else
                        {
                            data.addRow([chartsdata[i].PeriodDate, chartsdata[i].SumFirst]);
                        }
                    }

                    chart.draw(data, options);

                    barometer.chart.loadingOff();
                },
                error: function () {
                    barometer.chart.loadingOff();
                }
            });

        });
    },

    hotelControls: function ()
    {
        $("#ReviewOfDataFilterForm #DistanceBlock").show()
        $("#ReviewOfDataFilterForm #StarBlock").show()
        $("#ReviewOfDataFilterForm #NumberOfRoomsBlock").show()
        |$("#ReviewOfDataFilterForm #IsConferenceHallBlock").show()
    },

    hostelControls: function ()
    {
        $("#ReviewOfDataFilterForm #DistanceBlock").show()
        $("#ReviewOfDataFilterForm #StarBlock").hide()
        $("#ReviewOfDataFilterForm #NumberOfRoomsBlock").hide()
        $("#ReviewOfDataFilterForm #IsConferenceHallBlock").hide()
    },

    TICAndAirportControls: function ()
    {
        $("#ReviewOfDataFilterForm #DistanceBlock").hide();
        $("#ReviewOfDataFilterForm #StarBlock").hide()
        $("#ReviewOfDataFilterForm #NumberOfRoomsBlock").hide()
        $("#ReviewOfDataFilterForm #IsConferenceHallBlock").hide()
    },

    chageUserType: function()
    {
        var self = this;
        switch ($('#ReviewOfDataFilterForm #UserType').val()) {
            case barometer.config.userType.hotel:
                self.hotelControls();
                break;
            case barometer.config.userType.hostel:
                self.hostelControls();
                break;
            default:
                self.TICAndAirportControls();
        }

        if ($('#ReviewOfDataFilterForm #UserType').val() !== barometer.config.userType.airport) {
            $("#tableFirstGroup h4").text("Туристів");
            $("#tableSecondGroup h4").text("Ліжко-ночей");
        }
        else
        {
            $("#tableFirstGroup h4").text("Регулярні рейси");
            $("#tableSecondGroup h4").text("Чартерні рейси")
        }
    },

    getReport: function () {
        var self = this;

        $.fileDownload('/ReviewOfData/GetReviewOfDataReport?year=' + $("#reviewOfDataReport #ReportYear").val(), {
            httpMethod: "POST",
            successCallback: function () {
                self.reportLoadingOff();
            },
            failCallback: function () {
                self.reportLoadingOff();
            }
        });
    },

    reportLoadingOn: function () {
        $("#reviewOfDataReport #createReport").html(globalLoadingButton);
        $("#reviewOfDataReport #createReport").blur();
        $("#reviewOfDataReport #ReportYear, #reviewOfDataReport #createReport").attr("disabled", true);
    },

    reportLoadingOff: function () {
        var self = this;
        self.isSaveInProcess = false;
        $("#reviewOfDataReport #createReport").html('Звіт');
        $("#reviewOfDataReport #ReportYear, #reviewOfDataReport #createReport").attr("disabled", false);
    }
};