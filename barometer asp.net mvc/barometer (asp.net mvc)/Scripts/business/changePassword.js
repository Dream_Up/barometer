﻿$(document).ready(function () {
    barometer.changePassword.initialize();
});

var barometer = barometer || {};

barometer.changePassword = {
    
    initialize: function () {
        var self = this;
        $("#changePasswordLink").click(function () {
            $(this).html(globalLoadingButton);
            self._getChangePasswordViewModal();
        });
    },

    _getChangePasswordViewModal: function () {
        var self = this;
        $.ajax({
            url: '/Manage/ChangePassword',
            dataType: 'html',
            cache: false,
            type: 'GET',
            success: function (data) {
                $("#changePasswordModalPlace").html(data);
                var modal = $("#changePasswordModal").modal();
                $('#saveChangePassword').click(function () {
                    self._saveChangePassword();
                });

                modal.on('shown.bs.modal', function () {
                    $("#changePasswordLink").html('Змінити пароль');
                });
            }
        });
    },

    _saveChangePassword: function () {
        var self = this;

        var form = $('#changePasswordForm');
        $.validator.unobtrusive.parse(form);
        if (form.valid()) {
            self.loadingOn();
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            $.ajax({
                url: '/Manage/ChangePassword',
                type: 'POST',
                data: {
                    __RequestVerificationToken: token,
                    model: {
                        OldPassword: $('input[name="OldPassword"]', form).val(),
                        NewPassword: $('input[name="NewPassword"]', form).val(),
                        ConfirmPassword: $('input[name="ConfirmPassword"]', form).val()
                    }
                },
                success: function (result) {
                    if (result) {
                        if (result.SuccessMessage) {
                            toastr.success(result.SuccessMessage);
                            $("#changePasswordModal").modal('hide');
                        }
                        else if (result.ValidationMessage) {
                            $("[data-valmsg-for='OldPassword']", form).html('<span id="OldPassword-error">' + result.ValidationMessage + '</span>');
                            toastr.warning("Некоректне введення даних!");
                            self.loadingOff();
                        }
                        else {
                            self.loadingOff();
                        }
                    }
                },
                error: function (errorResult) {
                    self.loadingOff();
                }
            });
        }
        else {
            toastr.warning("Некоректне введення даних!");
        }
    },

    loadingOn: function () {
        $('#loadingChangePassword').show();
    },

    loadingOff: function () {
        $('#loadingChangePassword').hide();
    }
}