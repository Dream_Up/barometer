﻿$(document).ready(function () {
    barometer.index.initialize();
});

var barometer = barometer || {};

barometer.index = {
    
    initialize: function () {
        var self = this;

        $('#loginAuteficationForm').submit(function () {
            var form = $('#loginAuteficationForm');
            $.validator.unobtrusive.parse(form);
            if (form.valid()) {
                self.loadingOn();
                var token = $('input[name="__RequestVerificationToken"]', form).val();
                $.ajax({
                    url: $(this)[0].action,
                    type: 'POST',
                    data: {
                        __RequestVerificationToken: token,
                        model: {
                            Login: $('input[name="Login"]', form).val(),
                            Password: $('input[name="Password"]', form).val()
                        }
                    },
                    success: function (result) {
                        if (result) {
                            if (result.RedirectUrl) {
                                window.location.href = result.RedirectUrl;
                            }
                            else if (result.ValidationMessage) {
                                self.loadingOff();
                                $('#validationContent').show().html(result.ValidationMessage);
                                toastr.warning("Некоректне введення даних!");
                            }
                            else {
                                self.loadingOff();
                            }
                        }
                    },
                    error: function (errorResult) {
                        self.loadingOff();
                    }
                });
                return false;
            }
            else {
                toastr.warning("Некоректне введення даних!");
            }
        });
    },

    loadingOn: function () {
        $('#validationContent').hide().html('');
        $("#loginButton").html(globalLoadingButton);
        $("#loginButton").blur();
        $("#loginButton, #Login, #Password").attr("disabled", true);
    },

    loadingOff: function () {
        $("#loginButton").html('Увійти');
        $("#loginButton, #Login, #Password").attr("disabled", false);
    }
}