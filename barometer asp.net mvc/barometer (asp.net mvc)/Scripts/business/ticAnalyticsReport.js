﻿$(document).ready(function () {
    barometer.ticAnalyticsReport.initialize();
});

var barometer = barometer || {};

barometer.ticAnalyticsReport = {

    initialize: function () {
        var self = this;
        google.charts.load('current', { 'packages': ['corechart', 'table'] });

        barometer.reportChart.getChartComparisonOfVisitorsTEST(null, true, "chart_div", "");
        barometer.reportChart.getChartGeographyOfVisitors(null, true, "chartGeographyOfVisitorsCountry", "tableGeographyOfVisitorsCountry");
        barometer.reportChart.getChartGeographyOfVisitors(null, false, "chartGeographyOfVisitorsCityAndRegion", "tableGeographyOfVisitorsCityAndRegion");

        var users = JSON.parse($("#Users").val());
        $.each(users, function (key, value) {
            barometer.reportChart.getChartGeographyOfVisitors(value.UserId, true, "chartGeographyOfVisitorsCountry_" + value.UserId, "tableGeographyOfVisitorsCountry_" + value.UserId);
            barometer.reportChart.getChartComparisonOfVisitors(value.UserId, true, "chartComparisonOfVisitorsCountry_" + value.UserId, "tableComparisonOfVisitorsCountry_" + value.UserId);
            barometer.reportChart.getChartGeographyOfVisitors(value.UserId, false, "chartGeographyOfVisitorsCityAndRegion_" + value.UserId, "tableGeographyOfVisitorsCityAndRegion_" + value.UserId);
            barometer.reportChart.getChartComparisonOfVisitors(value.UserId, false, "chartComparisonOfVisitorsCityAndRegion_" + value.UserId, "tableComparisonOfVisitorsCityAndRegion_" + value.UserId);
            barometer.reportChart.getChartInterestsOfVisitors(value.UserId, "chartInterestsOfVisitors_" + value.UserId, "tableInterestsOfVisitors_" + value.UserId);
        });
    }
}