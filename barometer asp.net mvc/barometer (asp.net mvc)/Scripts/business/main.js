﻿$(document).ready(function () {
    $("#logOff").click(function () {
        $("#logOffPOST").submit();
    });
});

function clearTable(table) {
    table.clear();
    table.draw();
}

function getCommentsByPeriod(periodId, callback)
{
    $.ajax({
        url: '/Api/Comments/GetCommentsByPeriod?periodId=' + periodId,
        dataType: 'json',
        cache: false,
        type: 'GET',
        success: callback
    });
}

function validateQty(event) {
    var key = window.event ? event.keyCode : event.which;
    if (key < 48 || key > 57) {
        return false;
    }
    else return true;
};

function disAllowEmptyInComboBox(option, checked, select) {
    optionValue = $(option).val();
    if (select.val().length == 0) {
        if (!checked) {
            select.multiselect('select', [optionValue], true);
        }
    }
}

function disAllowDeselectAll(select)
{
    select.multiselect('select', select.find('option').val());
    select.multiselect('refresh');
}

function onlyNumberForValidateFields(e) {
    // Allow: backspace, delete, tab, escape, enter
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

globalLoadingButton = '<img src="/Content/images/loading_button.gif" width="20" height="20"/>';

//var globalErrorMessage = "Сталася помилка спробуйте, ще раз";