﻿$(document).ready(function () {
    barometer.statistic.initialize();
});

var barometer = barometer || {};

barometer.statistic = {
    
    initialize: function () {
        var self = this;

        var statisticsTable = $('#statisticsTable')
            .on('preXhr.dt', function (e, settings, data) {
                $('#statisticsTable tbody').html('<tr><td colspan="9" class="loading"></td></tr>');
                $("input, select, button").not("input[type='hidden']").prop("disabled", true);
            })
            .on('xhr.dt', function (e, settings, json, xhr) {
                $("input, select, button").prop("disabled", false);
            }).DataTable({
            scrollY: "500px",
            scrollCollapse: true,
            paging: false,
            info: false,
            language: {
                loadingRecords: "Завантаження...",
                emptyTable: "Завантаження...",
                zeroRecords: "За даними критеріями нічого не знайдено..."
            },
            ajax: {
                url: self._getStatisticsTableUrl(),
                type: "GET",
                cache: false
            },
            columns: [
                { data: "Type" },
                { data: "Stars" },
                { data: "Name" },
                { data: "PersonName" },
                { data: "Phone" },
                { data: "Email" },
                { data: "NumberOfRooms" },
                { data: "NumberOfBeds" },
                { data: "LastUpdatedDate" }
            ],
            columnDefs: [
               {
                   "targets": [8],
                   "type": "de_date"
               }
            ],
            createdRow: function (row, data, index) {
                if (!data["LastUpdatedDate"]) {
                    $('td', row).addClass('not-completed-data');
                }
            },
            initComplete: function () {
                this.api().columns('#typeColumn').every( function () {
                    var column = this;
                    var select = $("#typeFilter")
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
 
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
 
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                });
        }
        });

        $('#statisticsTable_filter').hide();

        $('#search').keyup(function (e) {
                statisticsTable.search(this.value).draw();
        });

        $('#Period').on("change", function () {
            clearTable(statisticsTable);
            statisticsTable.ajax.url(self._getStatisticsTableUrl()).load();
        })

        $('#refreshTable').click(function () {
            clearTable(statisticsTable);
            statisticsTable.ajax.reload();
        });

        $('#clearFilter').click(function () {
            $('#search').val('');
            $("#typeFilter").val('').trigger('change');
            statisticsTable.search('').draw();
        });
    },
    
    _getStatisticsTableUrl: function () {
        return "/Api/Statistics/GetStatisticsByPeriod?periodId=" + $("#Period").val();
    }

};