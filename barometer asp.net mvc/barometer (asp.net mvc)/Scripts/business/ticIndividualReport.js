﻿$(document).ready(function () {
    barometer.ticIndividualReport.initialize();
});

var barometer = barometer || {};

barometer.ticIndividualReport = {
    
    initialize: function () {
        var self = this;
        var userId = $("#UserId").val();
       
        google.charts.load('current', { 'packages': ['corechart', 'table'] });

        barometer.reportChart.getChartGeographyOfVisitors(userId, true, "chartGeographyOfVisitorsCountry", "tableGeographyOfVisitorsCountry");
        barometer.reportChart.getChartComparisonOfVisitors(userId, true, "chartComparisonOfVisitorsCountry", "tableComparisonOfVisitorsCountry");
        barometer.reportChart.getChartGeographyOfVisitors(userId, false, "chartGeographyOfVisitorsCityAndRegion", "tableGeographyOfVisitorsCityAndRegion");
        barometer.reportChart.getChartComparisonOfVisitors(userId, false, "chartComparisonOfVisitorsCityAndRegion", "tableComparisonOfVisitorsCityAndRegion");
        barometer.reportChart.getChartInterestsOfVisitors(userId, "chartInterestsOfVisitors", "tableInterestsOfVisitors");
    }
}