﻿$(document).ready(function () {
    barometer.comments.initialize();
});

var barometer = barometer || {};

barometer.comments = {

    initialize: function () {
        var self = this;

        var commentsTable = $('#commentsTable')
            .on('preXhr.dt', function (e, settings, data) {
                $('#commentsTable tbody').html('<tr><td colspan="9" class="loading"></td></tr>');
                $("input, select, button").not("input[type='hidden']").prop("disabled", true);
            })
            .on('xhr.dt', function (e, settings, json, xhr) {
                $("input, select, button").prop("disabled", false);

            })
            .on('draw.dt', function () {
                $('.dropdown-selector .edit').click(function () {
                    self._getCommentViewModal($(this).parent().attr("data-id"), commentsTable);
                });
                $('.dropdown-selector .delete').click(function () {
                    self.deleteComment($(this).parent().attr("data-id"), commentsTable);
                });
            }).DataTable({
                scrollY: "500px",
                scrollCollapse: true,
                paging: false,
                info: false,
                order: [[ 3, "desc"]],
                language: {
                    loadingRecords: "Завантаження...",
                    emptyTable: "Коментарі відсутні",
                    zeroRecords: "За даними критеріями нічого не знайдено..."
                },
                ajax: {
                    url: self._getCommentsTableUrl(),
                    type: "GET",
                    cache: false
                },
                columns: [
                    { data: "UserName" },
                    { data: "TypeDescription" },
                    { data: "Text" },
                    { data: "Created" },
                    { data: "Changed" },
                    { data: "Period"},
                    { data: "Action", width: "60px"}
                ],
                columnDefs: [
                   {
                       targets: [3,4,5],
                       type: "de_date"
                   },
                   {
                       className: "dt-center",
                       targets: 6
                   }
                ],
                initComplete: function () {
                    this.api().columns(1).every(function () {
                        var column = this;
                        var select = $("#typeFilter")
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });

        $('#commentsTable_filter').hide();

        $('#search').keyup(function (e) {
            commentsTable.search(this.value).draw();
        });

        $('#applyFilter').on("click", function () {
            const firstPeriodStr = $("#PeriodFirst").val();
            const secondPeriodStr = $("#PeriodSecond").val();

            if (parseInt(firstPeriodStr) <= parseInt(secondPeriodStr)) {
                clearTable(commentsTable);
                commentsTable.ajax.url(self._getCommentsTableUrl()).load();
            }
            else
            {
                toastr.warning('Не правильно вибрані періоди!');
            }
        })

        $('#refreshTable').click(function () {
            clearTable(commentsTable);
            commentsTable.ajax.reload();
        });

        $('#clearFilter').click(function () {
            $('#search').val('');
            $("#typeFilter").val('').trigger('change');
            commentsTable.search('').draw();
        });

        $('#addComment').click(function () {
            self._getCommentViewModal(0, commentsTable);
        });
    },

    _getCommentsTableUrl: function () {
        return "/Api/Comments/GetCommentsByPeriods?periodFirst=" + $("#PeriodFirst").val() + "&periodSecond=" + $("#PeriodSecond").val();
    },

    _getCommentViewModal: function (id, table)
    {
        var self = this;
        $.ajax({
            url: '/Operators/GetCommentModalView?id='+id,
            dataType: 'html',
            cache: false,
            type: 'GET',
            success: function (data)
            {
                $("#CommentModalPlace").html(data);
                $("#EditComment").modal();
                $('#saveComment').click(function () {
                    self.saveComment(table);
                });
            }
        });
    },

    saveComment: function(table)
    {
        $.validator.unobtrusive.parse($("#EditCommentForm"));
        if ($("#EditCommentForm").valid()) {
            $.ajax({
                url: '/Api/Comments/SaveComment',
                dataType: 'json',
                cache: false,
                type: 'POST',
                data: $("#EditCommentForm").serializeArray(),
                success: function () {
                    toastr.success("Дані успішно збережені!");
                    $("#EditComment").modal('toggle');
                    clearTable(table);
                    table.ajax.reload();
                }
            });
        }
        else {
            toastr.warning("Некоректне введення даних!");
        }
    },

    deleteComment: function (id, table) {
        $.ajax({
            url: '/Api/Comments/DeleteComment?id='+id,
            dataType: 'json',
            cache: false,
            type: 'POST',
            success: function () {
                toastr.success("Дані успішно видалені!");
                clearTable(table);
                table.ajax.reload();
            }
        });
    }
};