﻿$(document).ready(function () {
    barometer.feedback.initialize();
});

var barometer = barometer || {};

barometer.feedback = {
    
    initialize: function () {
        var self = this;

        $("#sentFeedback").click(function () {
            self.sent();
        });

        $('#FileUpload').bind('change', function () {
            if (this.files[0].size / 1024 > 10240)
            {
                $(this).val(null);
                toastr.warning("Максимально допустимий розмір 10МБ!");
            }
        });
    },

    sent: function () {
        $.validator.unobtrusive.parse($("#FeedbackForm"));
        if ($("#FeedbackForm").valid()) {
            var element = $("#FeedbackForm")[0];
            var form = new FormData(element);
            $.ajax({
                url: '/Feedback/Upload',
                data: form,
                dataType: 'json',
                contentType: false,
                processData: false,
                cache: false,
                type: 'POST',
                success: function (data) {
                    if (data.Warning) {
                        toastr.warning(data.Warning);
                    }
                    else if (data.Success) {
                        toastr.success(data.Success);
                        $("#FeedbackForm input, #FeedbackForm textarea").val(null);
                    }
                }
            });
        }
        else {
            toastr.warning("Форма не валідна!");
        }
    }
}