﻿using System.Web;
using System.Web.Optimization;

namespace barometer
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.validate.unobtrusive*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/bootstrap-multiselect.js"));

            bundles.Add(new ScriptBundle("~/bundles/dropzone").Include(
                        "~/Scripts/dropzone.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-datetimepicker.css",
                      "~/Content/bootstrap-multiselect.css",
                      "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/report").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-datetimepicker.css",
                      "~/Content/bootstrap-multiselect.css",
                      "~/Content/report.css"));

            bundles.Add(new StyleBundle("~/bundles/main").Include(
                     "~/Scripts/business/main.js",
                      "~/Scripts/business/config.js"
                     ));

            bundles.Add(new StyleBundle("~/bundles/statistics").Include(
                     "~/Scripts/business/statistics.js"));

            bundles.Add(new StyleBundle("~/bundles/infoGraphics").Include(
                     "~/Scripts/business/infoGraphics.js"));

            bundles.Add(new StyleBundle("~/bundles/reviewOfData").Include(
                     "~/Scripts/business/reviewOfData.js",
                     "~/Scripts/business/chart.js"));

            bundles.Add(new StyleBundle("~/bundles/benchmarking").Include(
                     "~/Scripts/business/benchmarking.js",
                     "~/Scripts/business/chart.js"));

            bundles.Add(new StyleBundle("~/bundles/feedback").Include(
                    "~/Scripts/business/feedback.js"));

            bundles.Add(new StyleBundle("~/bundles/comments").Include(
                    "~/Scripts/business/comments.js"));

            bundles.Add(new StyleBundle("~/bundles/insertData").Include(
                    "~/Scripts/business/insertData.js"));

            bundles.Add(new StyleBundle("~/bundles/ticData").Include(
                    "~/Scripts/business/ticData.js"));

            bundles.Add(new StyleBundle("~/bundles/ticReport").Include(
                    "~/Scripts/business/chart.js",
                    "~/Scripts/business/reportChart.js",
                    "~/Scripts/business/ticReport.js"
                    ));

            bundles.Add(new StyleBundle("~/bundles/ticIndividualReport").Include(
                "~/Scripts/business/chart.js",
                    "~/Scripts/business/reportChart.js",
                    "~/Scripts/business/ticIndividualReport.js"));

            bundles.Add(new StyleBundle("~/bundles/ticAnalyticsReport").Include(
                "~/Scripts/business/chart.js",
                    "~/Scripts/business/reportChart.js",
                    "~/Scripts/business/ticAnalyticsReport.js"));

            bundles.Add(new StyleBundle("~/bundles/index").Include(
                    "~/Scripts/business/index.js"));

            bundles.Add(new StyleBundle("~/bundles/changePassword").Include(
                    "~/Scripts/business/changePassword.js"));

            bundles.Add(new StyleBundle("~/bundles/register").Include(
                   "~/Scripts/business/register.js"));

            bundles.Add(new StyleBundle("~/bundles/editUserData").Include(
                   "~/Scripts/business/editUserData.js"));

        }
    }
}
