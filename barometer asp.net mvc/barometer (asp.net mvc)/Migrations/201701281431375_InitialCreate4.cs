namespace barometer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HistoryUserValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        Period_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Periods", t => t.Period_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Period_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HistoryUserValues", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.HistoryUserValues", "Period_Id", "dbo.Periods");
            DropIndex("dbo.HistoryUserValues", new[] { "User_Id" });
            DropIndex("dbo.HistoryUserValues", new[] { "Period_Id" });
            DropTable("dbo.HistoryUserValues");
        }
    }
}
