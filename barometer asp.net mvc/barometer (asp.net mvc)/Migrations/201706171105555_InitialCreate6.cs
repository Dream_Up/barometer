namespace barometer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate6 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TICDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CountTourists = c.Int(nullable: false),
                        Sex = c.Int(nullable: false),
                        Events = c.Int(nullable: false),
                        PrintingIndustry = c.Int(nullable: false),
                        Souvenirs = c.Int(nullable: false),
                        Excursions = c.Int(nullable: false),
                        Museums = c.Int(nullable: false),
                        Food = c.Int(nullable: false),
                        ReferenceInformation = c.Int(nullable: false),
                        Residence = c.Int(nullable: false),
                        DateVisit = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Changed = c.DateTime(nullable: false),
                        Region_Id = c.Int(),
                        City_Id = c.Int(),
                        Country_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Regions", t => t.Region_Id)
                .ForeignKey("dbo.Cities", t => t.City_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Region_Id)
                .Index(t => t.City_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Visible = c.Boolean(nullable: false),
                        SortPosition = c.Int(nullable: false),
                        Name = c.String(),
                        Alias = c.String(),
                        UserName = c.String(),
                        Created = c.DateTime(nullable: false),
                        Changed = c.DateTime(nullable: false),
                        Region_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Regions", t => t.Region_Id)
                .Index(t => t.Region_Id);
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Visible = c.Boolean(nullable: false),
                        SortPosition = c.Int(nullable: false),
                        Name = c.String(),
                        Alias = c.String(),
                        UserName = c.String(),
                        Created = c.DateTime(nullable: false),
                        Changed = c.DateTime(nullable: false),
                        Country_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Country_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TICDatas", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.TICDatas", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.TICDatas", "City_Id", "dbo.Cities");
            DropForeignKey("dbo.TICDatas", "Region_Id", "dbo.Regions");
            DropForeignKey("dbo.Regions", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Cities", "Region_Id", "dbo.Regions");
            DropIndex("dbo.Regions", new[] { "Country_Id" });
            DropIndex("dbo.Cities", new[] { "Region_Id" });
            DropIndex("dbo.TICDatas", new[] { "User_Id" });
            DropIndex("dbo.TICDatas", new[] { "Country_Id" });
            DropIndex("dbo.TICDatas", new[] { "City_Id" });
            DropIndex("dbo.TICDatas", new[] { "Region_Id" });
            DropTable("dbo.Regions");
            DropTable("dbo.Cities");
            DropTable("dbo.TICDatas");
        }
    }
}
