namespace barometer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate8 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TICDatas", "Transport", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TICDatas", "Transport");
        }
    }
}
