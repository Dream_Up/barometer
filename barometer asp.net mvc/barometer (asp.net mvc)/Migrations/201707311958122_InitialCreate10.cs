namespace barometer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate10 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HistoryUserValues", "Type", c => c.Int(nullable: false));
            DropColumn("dbo.HistoryUserValues", "HistoryUserValueEnum");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HistoryUserValues", "HistoryUserValueEnum", c => c.Int(nullable: false));
            DropColumn("dbo.HistoryUserValues", "Type");
        }
    }
}
