namespace barometer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "NumberOfConferencePosition", c => c.Int());
        }
        
        public override void Down()
        {   
            DropColumn("dbo.AspNetUsers", "NumberOfConferencePosition");
        }
    }
}
