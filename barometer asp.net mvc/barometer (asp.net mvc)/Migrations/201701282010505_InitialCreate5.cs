namespace barometer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationSettings",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 128),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Key);
            
            CreateTable(
                "dbo.InsertDataPermissions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Period_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Periods", t => t.Period_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Period_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InsertDataPermissions", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.InsertDataPermissions", "Period_Id", "dbo.Periods");
            DropIndex("dbo.InsertDataPermissions", new[] { "User_Id" });
            DropIndex("dbo.InsertDataPermissions", new[] { "Period_Id" });
            DropTable("dbo.InsertDataPermissions");
            DropTable("dbo.ApplicationSettings");
        }
    }
}
